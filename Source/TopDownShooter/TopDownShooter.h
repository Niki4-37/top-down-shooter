// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTopDownShooter, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogTDSNetwork, Log, All);
