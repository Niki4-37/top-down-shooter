// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TopDownShooter : ModuleRules
{
    public TopDownShooter(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "Niagara",
            "NavigationSystem",
            "AIModule",
            "PhysicsCore"
        });

        PublicIncludePaths.AddRange(new string[]
        {
            "TopDownShooter/Public/Player",
            "TopDownShooter/Public/FuncLibrary",
            "TopDownShooter/Public/Weapon",
            "TopDownShooter/Public/Components",
            "TopDownShooter/Public/Buff",
            "TopDownShooter/Public/Interfaces",
            "TopDownShooter/Public/UI",
            "TopDownShooter/Public/SupportingFiles"
        });
    }
}
