// This is educational project

#include "FuncLibrary/Types.h"
#include "Buff/TDSStateEffect.h"

DEFINE_LOG_CATEGORY_STATIC(Types_LOG, All, All);

void UTypes::ApplyStateEffectByPhysicalMaterial(AActor* ActorToApplyEffect, TSubclassOf<UTDSStateEffect> EffectToApply, UPhysicalMaterial* FoundPhysMat, FName BoneName)
{
    if (!ActorToApplyEffect || !FoundPhysMat || !EffectToApply) return;

    const auto CausingEffect = Cast<UTDSStateEffect>(EffectToApply->GetDefaultObject());
    if (!CausingEffect) return;

    const TArray<UPhysicalMaterial*> InteractionMaterials = CausingEffect->GetStateEffectData().PossibleInteractionMaterials;
    for (const auto InteractionMaterial : InteractionMaterials)
    {
        if (InteractionMaterial != FoundPhysMat) continue;

        const auto NewAppliedEffect = NewObject<UTDSStateEffect>(ActorToApplyEffect, EffectToApply);
        if (!NewAppliedEffect) continue;

        NewAppliedEffect->InitObject(ActorToApplyEffect, BoneName);
    }
}
