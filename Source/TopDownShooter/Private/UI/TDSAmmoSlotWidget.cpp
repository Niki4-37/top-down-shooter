// This is educational project

#include "UI/TDSAmmoSlotWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Styling/SlateColor.h"

void UTDSAmmoSlotWidget::InitAmmoSlot(const FAmmoSlot& Data)
{
    AmmoSlotData = Data;

    if (AmmoImage)
    {
        AmmoImage->SetBrushFromTexture(AmmoSlotData.AmmoIcon);
    }
    UpdateAmmoSlot(Data.CarriedAmmo);
}

void UTDSAmmoSlotWidget::UpdateAmmoSlot(int32 NewAmmo)
{
    AmmoSlotData.CarriedAmmo = NewAmmo;
    if (CarriedAmmoText)
    {
        CarriedAmmoText->SetText(FText::AsNumber(AmmoSlotData.CarriedAmmo));
    }

    const auto WidgetColor = AmmoSlotData.CarriedAmmo == 0  //
                                 ?                          //
                                 EmptyAmmoColor :           //
                                 (AmmoSlotData.CarriedAmmo == AmmoSlotData.MaxCarriedAmmo ? FullAmmoColor : NormalAmmoColor);
    SetColorAndOpacity(WidgetColor);
}
