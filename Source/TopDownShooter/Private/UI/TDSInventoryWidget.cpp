// This is educational project

#include "UI/TDSInventoryWidget.h"
#include "Components/TDSInventoryComponent.h"
#include "Components/VerticalBox.h"
#include "Components/HorizontalBox.h"
#include "UI/TDSWeaponSlotWidget.h"
#include "UI/TDSAmmoSlotWidget.h"
#include "TDSGameInstance.h"
#include "Player/TDSPlayerState.h"
#include "EngineUtils.h"

void UTDSInventoryWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GetOwningPlayer())
    {
        GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UTDSInventoryWidget::OnNewPawn);
        OnNewPawn(GetOwningPlayerPawn());
    }
}

void UTDSInventoryWidget::OnNewPawn(APawn* NewPawn)
{
    if (!NewPawn) return;
    const auto InventoryComponent = NewPawn->FindComponentByClass<UTDSInventoryComponent>();
    if (InventoryComponent && !InventoryComponent->OnWeaponEquipped.IsBoundToObject(this))
    {
        InventoryComponent->OnWeaponEquipped.AddUObject(this, &UTDSInventoryWidget::OnWeaponEquipped);
    }
    if (InventoryComponent && !InventoryComponent->OnInventoryWeaponInfoChanged.IsBoundToObject(this))
    {
        InventoryComponent->OnInventoryWeaponInfoChanged.AddUObject(this, &UTDSInventoryWidget::OnInventoryWeaponInfoChanged);
    }
    if (InventoryComponent && !InventoryComponent->OnAmmoInWeaponChanged.IsBoundToObject(this))
    {
        InventoryComponent->OnAmmoInWeaponChanged.AddUObject(this, &UTDSInventoryWidget::OnAmmoInWeaponChanged);
    }
    if (InventoryComponent && !InventoryComponent->OnReloadindStart.IsBoundToObject(this))
    {
        InventoryComponent->OnReloadindStart.AddUObject(this, &UTDSInventoryWidget::OnReloadingStart);
    }
    if (InventoryComponent && !InventoryComponent->OnReloadindEnd.IsBoundToObject(this))
    {
        InventoryComponent->OnReloadindEnd.AddUObject(this, &UTDSInventoryWidget::OnReloadingEnd);
    }
    if (InventoryComponent && !InventoryComponent->OnInventoryAmmoChanged.IsBoundToObject(this))
    {
        InventoryComponent->OnInventoryAmmoChanged.AddUObject(this, &UTDSInventoryWidget::OnInventoryAmmoChanged);
    }
    InitSlots();
}

void UTDSInventoryWidget::InitSlots()
{
    const auto InventoryComponent = GetOwningPlayerPawn()->FindComponentByClass<UTDSInventoryComponent>();
    if (!InventoryComponent) return;
    // TArray<FWeaponSlot> WeaponSlots = InventoryComponent->GetWeaponSlots();
    // TArray<FAmmoSlot> AmmoSlots = InventoryComponent->GetAmmoSlots();

    if (WeaponSlotsBox)
    {
        WeaponSlotsBox->ClearChildren();

        for (const auto& WeaponSlotData : InventoryComponent->GetWeaponSlots())
        {
            const auto WeaponSlotWidget = CreateWidget<UTDSWeaponSlotWidget>(GetWorld(), WeaponSlotWidgetClass);
            if (!WeaponSlotWidget) continue;

            WeaponSlotWidget->InitWeaponSlot(WeaponSlotData);

            WeaponSlotsBox->AddChild(WeaponSlotWidget);
        }
    }
    if (AmmoSlotsBox)
    {
        AmmoSlotsBox->ClearChildren();

        for (const auto& AmmoSlotData : InventoryComponent->GetAmmoSlots())
        {
            const auto AmmoSlotWidget = CreateWidget<UTDSAmmoSlotWidget>(GetWorld(), AmmoSlotWidgetClass);
            if (!AmmoSlotWidget) continue;

            AmmoSlotWidget->InitAmmoSlot(AmmoSlotData);

            AmmoSlotsBox->AddChild(AmmoSlotWidget);
        }
    }
    OnWeaponEquipped(0);  // OnWeaponEquipped(ActiveWeaponSlotIndex); // for loading inventory
    UE_LOG(LogTemp, Display, TEXT("InventoryWidget initialized"));
}

void UTDSInventoryWidget::OnWeaponEquipped(int8 ActiveWeaponSlot)
{
    UE_LOG(LogTemp, Display, TEXT("InventoryWidget::OnWeaponEquipped from slot: %i"), ActiveWeaponSlot);
    // ActiveWeaponSlotIndex = ActiveWeaponSlot; // for loading inventory
    if (!WeaponSlotsBox) return;
    uint8 CurrentSlotIndex{0};
    for (const auto& Element : WeaponSlotsBox->GetAllChildren())
    {
        const auto WeaponSlotWidget = Cast<UTDSWeaponSlotWidget>(Element);
        if (!WeaponSlotWidget) continue;

        const bool bIsActivWeapon = CurrentSlotIndex == ActiveWeaponSlot;
        WeaponSlotWidget->ToggleWeaponStatus(bIsActivWeapon);
        ++CurrentSlotIndex;
    }
}

void UTDSInventoryWidget::OnInventoryAmmoChanged(EWeaponType Type, int32 NewAmmo)
{
    if (!AmmoSlotsBox) return;
    for (const auto Widget : AmmoSlotsBox->GetAllChildren())
    {
        const auto AmmoSlotWidget = Cast<UTDSAmmoSlotWidget>(Widget);
        if (!AmmoSlotWidget || AmmoSlotWidget->GetWeaponType() != Type) continue;

        AmmoSlotWidget->UpdateAmmoSlot(NewAmmo);
    }
}

void UTDSInventoryWidget::OnInventoryWeaponInfoChanged(uint8 WeaponSlotIndex, const FWeaponSlot& Data)
{
    if (!WeaponSlotsBox) return;

    const auto WeaponSlotWidget = Cast<UTDSWeaponSlotWidget>(WeaponSlotsBox->GetChildAt(WeaponSlotIndex));
    if (!WeaponSlotWidget) return;

    UE_LOG(LogTemp, Display, TEXT("NewWeapon: %s"), *Data.WeaponName.ToString());
    WeaponSlotWidget->InitWeaponSlot(Data);
}

void UTDSInventoryWidget::OnAmmoInWeaponChanged(uint8 WeaponSlotIndex, int32 NewAmmo)
{
    if (!WeaponSlotsBox) return;

    const auto WeaponSlotWidget = Cast<UTDSWeaponSlotWidget>(WeaponSlotsBox->GetChildAt(WeaponSlotIndex));
    if (!WeaponSlotWidget) return;

    WeaponSlotWidget->UpdateAmmoCapacity(NewAmmo);
}

void UTDSInventoryWidget::OnReloadingStart(uint8 WeaponSlotIndex)
{
    if (!WeaponSlotsBox) return;

    const auto WeaponSlotWidget = Cast<UTDSWeaponSlotWidget>(WeaponSlotsBox->GetChildAt(WeaponSlotIndex));
    if (!WeaponSlotWidget) return;

    WeaponSlotWidget->StartReloadingProgress();
}

void UTDSInventoryWidget::OnReloadingEnd(uint8 WeaponSlotIndex)
{
    if (!WeaponSlotsBox) return;

    const auto WeaponSlotWidget = Cast<UTDSWeaponSlotWidget>(WeaponSlotsBox->GetChildAt(WeaponSlotIndex));
    if (!WeaponSlotWidget) return;

    WeaponSlotWidget->StopReloadingProgress();
}
