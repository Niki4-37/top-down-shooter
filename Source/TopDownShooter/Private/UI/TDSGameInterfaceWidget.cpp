// This is educational project

#include "UI/TDSGameInterfaceWidget.h"
#include "Components/HorizontalBox.h"

void UTDSGameInterfaceWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GetOwningPlayer())
    {
        GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UTDSGameInterfaceWidget::OnNewPawn);
        OnNewPawn(GetOwningPlayerPawn());
    }
}

void UTDSGameInterfaceWidget::OnNewPawn(APawn* NewPawn) {}
