// This is educational project

#include "UI/TDSWeaponSlotWidget.h"
#include "Components/ProgressBar.h"
#include "Components/Image.h"
#include "TDSGameInstance.h"

void UTDSWeaponSlotWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();
}

void UTDSWeaponSlotWidget::InitWeaponSlot(const FWeaponSlot& Data)
{
    WeaponSlotData = Data;
    if (!GetWorld()) return;
    const auto GI = GetWorld()->GetGameInstance<UTDSGameInstance>();
    if (!GI) return;

    FWeaponData WeaponData;
    if (GI->GetWeaponDataByName(WeaponSlotData.WeaponName, WeaponData))
    {
        WeaponSlotData.WeaponIcon = WeaponData.WeaponIcon;
        WeaponSlotData.MaxAmmo = WeaponData.MaxAmmo;
        WeaponReloadingTime = WeaponData.ReloadTime;
    }

    UpdateAmmoCapacity(Data.AdditionalData.AmmoInWeapon);

    if (WeaponImage)
    {
        if (!WeaponSlotData.WeaponIcon)
        {
            WeaponImage->SetVisibility(ESlateVisibility::Hidden);
        }
        else
        {
            WeaponImage->SetVisibility(ESlateVisibility::Visible);
            WeaponImage->SetBrushFromTexture(WeaponSlotData.WeaponIcon);
        }
    }
    UE_LOG(LogTemp, Display, TEXT("WeaponSlot with: %s"), *WeaponSlotData.WeaponName.ToString());
}

void UTDSWeaponSlotWidget::UpdateAmmoCapacity(int32 NewAmmo)
{
    WeaponSlotData.AdditionalData.AmmoInWeapon = NewAmmo;
    if (AmmoCapacity)
    {
        UE_LOG(LogTemp, Display, TEXT("Ammo in weapon: %i, MaxAmmo: %i"), NewAmmo, WeaponSlotData.MaxAmmo);
        WeaponSlotData.MaxAmmo > 0 ? AmmoCapacity->SetPercent(static_cast<float>(NewAmmo) / WeaponSlotData.MaxAmmo) : AmmoCapacity->SetPercent(0.f);
    }
}

void UTDSWeaponSlotWidget::StartReloadingProgress()
{
    if (!GetWorld()) return;
    GetWorld()->GetTimerManager().SetTimer(ReloadingTimer, this, &UTDSWeaponSlotWidget::UpdateProgress, 0.02f, true);
    GetWorld()->GetTimerManager().SetTimer(ReloadingOverTimer, this, &UTDSWeaponSlotWidget::StopReloadingProgress, WeaponReloadingTime);
}

void UTDSWeaponSlotWidget::UpdateProgress()
{
    if (!WeaponReloadingTime || !AmmoCapacity)
    {
        GetWorld()->GetTimerManager().ClearTimer(ReloadingTimer);
        GetWorld()->GetTimerManager().ClearTimer(ReloadingOverTimer);
        return;
    }

    AmmoCapacity->SetPercent(GetWorld()->GetTimerManager().GetTimerElapsed(ReloadingOverTimer) / WeaponReloadingTime);
}

void UTDSWeaponSlotWidget::StopReloadingProgress()
{
    GetWorld()->GetTimerManager().ClearTimer(ReloadingTimer);
    GetWorld()->GetTimerManager().ClearTimer(ReloadingOverTimer);

    if (AmmoCapacity)
    {
        WeaponSlotData.MaxAmmo > 0 ? AmmoCapacity->SetPercent(static_cast<float>(WeaponSlotData.AdditionalData.AmmoInWeapon) / WeaponSlotData.MaxAmmo) : AmmoCapacity->SetPercent(0.f);
    }
}

void UTDSWeaponSlotWidget::ToggleWeaponStatus(bool bIsArmed)
{
    if (AmmoCapacity)
    {
        const auto ColorToSet = bIsArmed ? WeaponInArmsColor : WeaponInHolsterColor;
        AmmoCapacity->SetFillColorAndOpacity(ColorToSet);
    }
}
