// This is educational project

#include "Player/TDSPlayerController.h"
#include "Components/TDSRespawnComponent.h"
#include "GameFramework/SpectatorPawn.h"
#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"

#include "TDSGameStateBase.h"

ATDSPlayerController::ATDSPlayerController()
{
    RespawnComponent = CreateDefaultSubobject<UTDSRespawnComponent>("RespawnComponent");
}

bool ATDSPlayerController::IsPlayerSpectator() const
{
    if (!PlayerState) return false;
    return PlayerState->IsSpectator();
}

void ATDSPlayerController::OnRep_Pawn()
{
    Super::OnRep_Pawn();

    if (GetStateName() == NAME_Playing)
    {
        ApplyCameraEffects(FVector(1.f), FLinearColor::White);
    }

    if (GetStateName() == NAME_Spectating)
    {
        if (PawnToAttach)
        {
            UE_LOG(LogTemp, Warning, TEXT("PawnToAttach: %s"), *PawnToAttach->GetName());
            SetViewTargetWithBlend(PawnToAttach, 1.f, VTBlend_Linear, 0.f, true);
        }
        else
        {
            AutoManageActiveCameraTarget(GetSpectatorPawn());
        }
    }
}

void ATDSPlayerController::StartSpectating()
{
    /* handled on server */
    if (!HasAuthority()) return;

    PlayerState->SetIsSpectator(true);
    ChangeState(NAME_Spectating);

    PawnToAttach = GetPawnToAttachSpectator();
    if (PawnToAttach)
    {
        SetViewTargetWithBlend(PawnToAttach, 1.f, VTBlend_Linear, 0.f, true);
    }
    else
    {
        AutoManageActiveCameraTarget(GetSpectatorPawn());
    }

    OnPlayerSpectator();

    StartSpectating_OnClient();
}

void ATDSPlayerController::StartSpectating_OnClient_Implementation()
{
    ApplyCameraEffects(FVector(0.1f, 0.0f, 0.0f), FLinearColor::White);

    ChangeState(NAME_Spectating);
}

void ATDSPlayerController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    if (GetStateName() == NAME_Playing)
    {
        ApplyCameraEffects(FVector(1.f), FLinearColor::White);
    }
}

void ATDSPlayerController::OnUnPossess()
{
    Super::OnUnPossess();
}

void ATDSPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSPlayerController, PawnToAttach);
}

APawn* ATDSPlayerController::GetPawnToAttachSpectator()
{
    if (!GetWorld()) return nullptr;

    const auto TDSGameState = Cast<ATDSGameStateBase>(GetWorld()->GetGameState());
    if (!TDSGameState) return nullptr;

    return TDSGameState->GetFirstAlivePawn();
}

void ATDSPlayerController::ApplyCameraEffects(const FVector& NewColorScale, const FLinearColor& FadeColor)
{
    if (!PlayerCameraManager) return;
    PlayerCameraManager->SetDesiredColorScale(NewColorScale, 1.f);
    PlayerCameraManager->StartCameraFade(1.f, 0.f, 1.5f, FadeColor, false, true);
}
