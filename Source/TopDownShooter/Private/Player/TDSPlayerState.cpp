// This is educational project

#include "Player/TDSPlayerState.h"
#include "Net/UnrealNetwork.h"

void ATDSPlayerState::DecreaseLive()
{
    /* handled on server from gamemode */
    UpdateLives(true);
}

void ATDSPlayerState::IncreaseLive()
{
    UpdateLives(false);
}

void ATDSPlayerState::AddScorePoints(int32 PointsToAdd)
{
    /* handled on server */
    ScorePoints += PointsToAdd;
    OnScoreChanged.Broadcast(ScorePoints);
};

void ATDSPlayerState::SaveInventory(const TArray<FWeaponSlot>& WeaponsSlots, const TArray<FAmmoSlot>& AmmoSlots)
{
    Weapons = WeaponsSlots;
    Ammo = AmmoSlots;
}

void ATDSPlayerState::LoadInventory(TArray<FWeaponSlot>& WeaponsSlots, TArray<FAmmoSlot>& AmmoSlots)
{
    WeaponsSlots = Weapons;
    AmmoSlots = Ammo;
}

void ATDSPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSPlayerState, Lives);
    DOREPLIFETIME(ATDSPlayerState, ScorePoints);
}

void ATDSPlayerState::UpdateLives(bool bIsDecreased)
{
    bIsDecreased ? --Lives : ++Lives;
    Lives = FMath::Clamp(Lives, 0, MaxLives);
    OnLivesChanged.Broadcast(Lives);
    if (!Lives)
    {
        GameOver_OnClient();
    }
}

void ATDSPlayerState::LivesChanged_OnRep()
{
    OnLivesChanged.Broadcast(Lives);
}

void ATDSPlayerState::ScorePointsChanged_OnRep()
{
    OnScoreChanged.Broadcast(ScorePoints);
}

void ATDSPlayerState::GameOver_OnClient_Implementation()
{
    OnGameOver.Broadcast();
}
