// This is educational project

#include "Player/TDSCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Components/DecalComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Player/TDSPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Components/TDSInventoryComponent.h"
#include "Components/TDSPlayerHealthComponent.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(TDSCharacter_LOG, All, All);

ATDSCharacter::ATDSCharacter()
{
    bReplicates = true;
    PrimaryActorTick.bCanEverTick = true;

    SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
    SpringArm->SetupAttachment(GetRootComponent());
    SpringArm->bUsePawnControlRotation = false;
    SpringArm->TargetArmLength = 1000.f;
    SpringArm->SetUsingAbsoluteRotation(true);

    Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
    Camera->SetupAttachment(SpringArm);

    InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>("InventoryComponent");
    InventoryComponent->SetIsReplicated(true);

    HealthComponent = CreateDefaultSubobject<UTDSPlayerHealthComponent>("HealthComponent");
    HealthComponent->SetIsReplicated(true);
}

void ATDSCharacter::BeginPlay()
{
    Super::BeginPlay();
    check(GetWorld());
    check(GetMesh());
    check(SpringArm);
    check(InventoryComponent);
    check(HealthComponent);

    CurrentSpringArmLenght = SpringArm->TargetArmLength;

    bIsAlive = true;
    bCanMoveCursore = true;

    SpawnDecalAtLocationForLocalPlayer();
}

void ATDSCharacter::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    if (HealthComponent)
    {
        HealthComponent->OnDeath.AddDynamic(this, &ATDSCharacter::OnDeath);  // was not dynamic
        HealthComponent->OnStaminaIsOver.AddUObject(this, &ATDSCharacter::OnStaminaIsOver);
        HealthComponent->OnShieldChanged.AddDynamic(this, &ATDSCharacter::OnShieldChanged);
    }
    if (InventoryComponent)
    {
        InventoryComponent->WeaponMakeShot.AddUObject(this, &ATDSCharacter::WeaponMakeShot);
        InventoryComponent->ReloadingAnimationStarts.AddUObject(this, &ATDSCharacter::ReloadingAnimationStarts);
    }
}

void ATDSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSCharacter, MovementState);
    DOREPLIFETIME(ATDSCharacter, bCanMoveCursore);
    DOREPLIFETIME(ATDSCharacter, bIsAiming);
}

void ATDSCharacter::Restart()
{
    Super::Restart();
    if (!HasAuthority()) return;
    InventoryComponent->LoadInventory();
}

void ATDSCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (CursorToWorld)
    {
        const auto PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
        if (PlayerController && PlayerController->IsLocalPlayerController())
        {
            FHitResult TraceHitResult;
            PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, TraceHitResult);
            const FRotator CursorRotation(TraceHitResult.ImpactNormal.Rotation());
            CursorToWorld->SetWorldLocationAndRotation(TraceHitResult.Location, CursorRotation);
        }
    }

    TickMovement(DeltaTime);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis("MoveForward", this, &ATDSCharacter::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &ATDSCharacter::MoveRight);

    PlayerInputComponent->BindAxis("ChangeCameraHeight", this, &ATDSCharacter::ChangeSpringArmLenght);

    PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &ATDSCharacter::OnStartWalking);
    PlayerInputComponent->BindAction("Walk", IE_Released, this, &ATDSCharacter::OnStopWalking);

    PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ATDSCharacter::OnStartSprint);
    PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ATDSCharacter::OnStopSprint);

    PlayerInputComponent->BindAction("Aiming", IE_Pressed, this, &ATDSCharacter::OnStartAiming);
    PlayerInputComponent->BindAction("Aiming", IE_Released, this, &ATDSCharacter::OnStoptAiming);

    DECLARE_DELEGATE_OneParam(FOnFiringSignature, bool);
    PlayerInputComponent->BindAction<FOnFiringSignature>("Fire", IE_Pressed, this, &ATDSCharacter::CharacterAttackEvent, true);
    PlayerInputComponent->BindAction<FOnFiringSignature>("Fire", IE_Released, this, &ATDSCharacter::CharacterAttackEvent, false);

    PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ATDSCharacter::TryToReloadWeapon);

    PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, InventoryComponent, &UTDSInventoryComponent::TryToSwitchToNextWeapon);
    PlayerInputComponent->BindAction("PrevWeapon", IE_Pressed, InventoryComponent, &UTDSInventoryComponent::TryToSwitchToPrevWeapon);
}

void ATDSCharacter::DropWeapon_BP_Implementation(UMeshComponent* WeaponMesh, const FWeaponSlot& WeaponSlotInfo) {}

void ATDSCharacter::MoveForward(float Amount)
{
    AddMovementInput(FVector(1.f, 0.f, 0.f), Amount);
    ChangeMovementState();
}

void ATDSCharacter::MoveRight(float Amount)
{
    AddMovementInput(FVector(0.f, 1.f, 0.f), Amount);
    ChangeMovementState();
}

void ATDSCharacter::ChangeSpringArmLenght(float Amount)
{
    if (FMath::IsNearlyZero(Amount) || bInProgress) return;
    UE_LOG(TDSCharacter_LOG, Display, TEXT("changin camera height"));
    bInProgress = true;
    const float NewTargetArmLenght = SpringArm->TargetArmLength + Amount;
    SpringArm->TargetArmLength = FMath::Clamp(NewTargetArmLenght, MinSpringArmLenght, MaxSpringArmLenght);

    if (!bCanChange) return;
    const float NewCurrentSpringArmLenght = DeltaLenght * Amount + CurrentSpringArmLenght;
    CurrentSpringArmLenght = FMath::Clamp(NewCurrentSpringArmLenght, MinSpringArmLenght, MaxSpringArmLenght);
    bCanChange = false;

    GetWorld()->GetTimerManager().SetTimer(SmoothLenghtTimet, this, &ATDSCharacter::SmoothSpringArmChangingLenght, RateDeltaLenght, true);
}

void ATDSCharacter::SmoothSpringArmChangingLenght()
{
    const int8 Modifier = SpringArm->TargetArmLength < CurrentSpringArmLenght ? 1 : -1;

    SpringArm->TargetArmLength += Modifier * DeltaLenght * StepDeltaLenght;

    if (Modifier > 0 && SpringArm->TargetArmLength >= CurrentSpringArmLenght)
    {
        GetWorld()->GetTimerManager().ClearTimer(SmoothLenghtTimet);
        bInProgress = false;
        bCanChange = true;
    }

    if (Modifier < 0 && SpringArm->TargetArmLength <= CurrentSpringArmLenght)
    {
        GetWorld()->GetTimerManager().ClearTimer(SmoothLenghtTimet);
        bInProgress = false;
        bCanChange = true;
    }
}

void ATDSCharacter::SpawnDecalAtLocationForLocalPlayer()
{
    const bool bIsLocalPlayer = GetLocalRole() == ENetRole::ROLE_AutonomousProxy || GetLocalRole() == ENetRole::ROLE_Authority;
    if (GetNetMode() == ENetMode::NM_DedicatedServer || !bIsLocalPlayer) return;

    CursorToWorld = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
}

void ATDSCharacter::TickMovement(float DeltaTime)
{
    UpdatePlayerRotation();
}

void ATDSCharacter::UpdatePlayerRotation()
{
    if (!bCanMoveCursore) return;

    const auto PlayerController = Cast<APlayerController>(Controller);
    if (!PlayerController || !PlayerController->IsLocalPlayerController()) return;

    FHitResult HitResult;
    /*  ECC_GameTraceChannel1 - LandscapeCursor */
    PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, true, HitResult);

    FVector PlayerLocation, CursorLocation;
    float CameraCorrection = 0;
    if (HitResult.bBlockingHit)
    {
        PlayerLocation = GetActorLocation();
        CursorLocation = HitResult.Location;
    }
    else
    {
        FVector2D PlayerScreenPosition, MouseScreenPosition;
        GetActorAndCursorScreenPosition(PlayerController, PlayerScreenPosition, MouseScreenPosition);

        PlayerLocation = FVector(PlayerScreenPosition, 0.f);
        CursorLocation = FVector(MouseScreenPosition, 0.f);

        CameraCorrection = 90.f;
    }

    float YawValue = UKismetMathLibrary::FindLookAtRotation(PlayerLocation, CursorLocation).Yaw + CameraCorrection;
    SetActorRotation(FQuat(FRotator(0.f, YawValue, 0.f)));
    SetActorRotationByYaw_OnServer(YawValue);
}

void ATDSCharacter::GetActorAndCursorScreenPosition(APlayerController* PlayerController,  //
                                                    FVector2D& ActorScreenPosition,       //
                                                    FVector2D& MouseScreenPosition)
{
    if (!PlayerController || !PlayerController->GetLocalPlayer())
    {
        ActorScreenPosition = FVector2D::ZeroVector;
        MouseScreenPosition = FVector2D::ZeroVector;
        return;
    }

    PlayerController->ProjectWorldLocationToScreen(GetActorLocation(), ActorScreenPosition, true);

    PlayerController->GetLocalPlayer()->ViewportClient->GetMousePosition(MouseScreenPosition);
}

void ATDSCharacter::CharacterUpdate()
{
    float ResultSpeed = MovementInfo.RunSpeed;
    // clang-format off
    switch (MovementState)
    {
        case EMovementState::Aim_State:     ResultSpeed = MovementInfo.AimSpeed; break;
        case EMovementState::AimWalk_State: ResultSpeed = MovementInfo.AimWalkSpeed; break;
        case EMovementState::Walk_State:    ResultSpeed = MovementInfo.WalkSpeed; break;
        case EMovementState::Run_State:     ResultSpeed = MovementInfo.RunSpeed; break;
        case EMovementState::Sprint_State:  ResultSpeed = MovementInfo.SprintSpeed; break;
        default:                            ResultSpeed = MovementInfo.RunSpeed; break;
    }
    // clang-format on
    GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;

    HealthComponent->UpdateStaminaState(MovementState);
}

void ATDSCharacter::ChangeMovementState()
{
    auto State = MovementState;
    const float DeviationFromForvardVector = 30.f;

    if (bWantsToSprint &&                                         //
        !bWantsToWalk &&                                          //
        !bIsAiming &&                                             //
        !GetVelocity().IsZero() &&                                //
        GetMovementDirection() >= -DeviationFromForvardVector &&  //
        GetMovementDirection() <= DeviationFromForvardVector &&   //
        bCanRun /*!FMath::IsNearlyZero(CurrentStamina)*/)
    {
        State = EMovementState::Sprint_State;
    }
    else if (bWantsToWalk && !bWantsToSprint && !bIsAiming)
    {
        State = EMovementState::Walk_State;
    }
    else if (bWantsToWalk && bIsAiming && !bWantsToSprint)
    {
        State = EMovementState::AimWalk_State;
    }
    else if (bIsAiming && !bWantsToSprint && !bWantsToWalk)
    {
        State = EMovementState::Aim_State;
    }
    else
    {
        State = EMovementState::Run_State;
    }

    if (State != MovementState)
    {
        SetMovementState_OnServer(State);

        InventoryComponent->UpdateStateWeapon(MovementState);
    }
}

float ATDSCharacter::GetMovementDirection() const
{
    if (GetVelocity().IsZero()) return 0.f;

    const auto VelocityNormal = GetVelocity().GetSafeNormal();
    const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
    const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);
    const auto Degrees = FMath::RadiansToDegrees(AngleBetween);

    return CrossProduct.IsZero() ? Degrees : Degrees * FMath::Sign(CrossProduct.Z);
}

void ATDSCharacter::OnStartWalking()
{
    bWantsToSprint = false;
    bWantsToWalk = true;
    ChangeMovementState();
}

void ATDSCharacter::OnStopWalking()
{
    bWantsToWalk = false;
    ChangeMovementState();
}

void ATDSCharacter::OnStartSprint()
{
    bWantsToSprint = true;
    bWantsToWalk = false;

    InventoryComponent->SetWeaponStartFire(false);

    ChangeMovementState();
}

void ATDSCharacter::OnStopSprint()
{
    bWantsToSprint = false;

    ChangeMovementState();
}

void ATDSCharacter::OnStartAiming()
{
    bWantsToSprint = false;
    SetAimingState_OnServer(true);

    ChangeMovementState();
}

void ATDSCharacter::OnStoptAiming()
{
    SetAimingState_OnServer(false);

    ChangeMovementState();
}

void ATDSCharacter::TryToReloadWeapon()
{
    if (MovementState == EMovementState::Sprint_State) return;
    UE_LOG(TDSCharacter_LOG, Display, TEXT("Character wants Reload"));
    InventoryComponent->TryToReloadWeapon();
}

void ATDSCharacter::CharacterAttackEvent(bool bIsFiring)
{
    const bool bCanAttack = MovementState != EMovementState::Sprint_State && bIsFiring && bCanMoveCursore;
    InventoryComponent->SetWeaponStartFire(bCanAttack);
}

void ATDSCharacter::OnDeath()
{
    /* from delegate which handled on server */
    if (!bIsAlive) return;

    UE_LOG(TDSCharacter_LOG, Display, TEXT("Character dead"));

    bCanMoveCursore = false;
    float TimeToSwitch{0.1f};
    const auto RandIndex = FMath::RandHelper(DeathAnimations.Num());
    if (DeathAnimations.IsValidIndex(RandIndex))
    {
        TimeToSwitch = DeathAnimations[RandIndex] ? (DeathAnimations[RandIndex]->CalculateSequenceLength() - 0.8f) : TimeToSwitch;  // GetSectionLength(int32 SectionIndex)
        PlayAnimMontage_Multicast(DeathAnimations[RandIndex]);
    }

    DestroyDecal_OnClient();

    DisableInput(Cast<APlayerController>(Controller));

    GetCharacterMovement()->DisableMovement();
    InventoryComponent->SaveInventory();
    InventoryComponent->SetWeaponStartFire(false);

    GetWorldTimerManager().SetTimer(SwitchToRagdollTimer, this, &ATDSCharacter::SwitchToRagdoll, TimeToSwitch, false);

    if (const auto PC = Cast<ATDSPlayerController>(Controller))
    {
        PC->StartSpectating();
    }

    bIsAlive = false;
}

void ATDSCharacter::SwitchToRagdoll()
{
    SetLifeSpan(10.f);
    GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    GetMesh()->SetCollisionObjectType(ECollisionChannel::ECC_PhysicsBody);
    GetMesh()->SetSimulatePhysics(true);
    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetWorldTimerManager().ClearTimer(SwitchToRagdollTimer);
}

void ATDSCharacter::OnStaminaIsOver(bool bIsStaminaOver)
{
    bCanRun = !bIsStaminaOver;
}

void ATDSCharacter::OnShieldChanged(float ShieldCapacity)
{
    bHasShield = !FMath::IsNearlyZero(ShieldCapacity);
}

void ATDSCharacter::ReloadingAnimationStarts(UAnimMontage* ReloadAnimation)
{
    PlayAnimMontage_Multicast(ReloadAnimation);
}

void ATDSCharacter::WeaponMakeShot(EWeaponShotType ShotType)
{
    /* from delegate handled on server */
    FString WeaponAnimationName;
    // clang-format off
    switch (ShotType)
    {
        case EWeaponShotType::LightShot:    WeaponAnimationName = "LightShot"; break;
        case EWeaponShotType::MediumShot:   WeaponAnimationName = "MediumShot"; break;
        case EWeaponShotType::HeavyShot:    WeaponAnimationName = "HeavyShot"; break;
        default:                            WeaponAnimationName = "LightShot";
    }
    // clang-format off
    const FString AnimationKey = (bIsAiming ? "Aiming" : "") + WeaponAnimationName;
    const auto FoundAnimationPtr = ShootingAnimationMap.Find(AnimationKey);
    if (!FoundAnimationPtr) return;
    const auto FoundAnimation = *FoundAnimationPtr;
    if (!FoundAnimation) return;
    PlayAnimMontage_Multicast(FoundAnimation);
}

void ATDSCharacter::DestroyDecal_OnClient_Implementation()
{
    if (CursorToWorld)
    {
        CursorToWorld->DestroyRenderState_Concurrent();
    }
}

void ATDSCharacter::PlayAnimMontage_Multicast_Implementation(UAnimMontage* Animation)
{
    PlayAnimMontage(Animation);
}

void ATDSCharacter::StopAnimMontage_Multicast_Implementation(UAnimMontage* Animation)
{
    StopAnimMontage(Animation);
}

void ATDSCharacter::SetActorRotationByYaw_OnServer_Implementation(float YawValue)
{
    SetActorRotationByYaw_Multicast(YawValue);
}

void ATDSCharacter::SetActorRotationByYaw_Multicast_Implementation(float YawValue)
{
    if (Controller && !Controller->IsLocalPlayerController())
    {
        SetActorRotation(FQuat(FRotator(0.f, YawValue, 0.f)));
    }
}

void ATDSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewMovementState)
{
    SetMovementState_Multicast(NewMovementState);
}

void ATDSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewMovementState)
{
    MovementState = NewMovementState;
    CharacterUpdate();
}

void ATDSCharacter::SetAimingState_OnServer_Implementation(bool bEnabled)
{
    bIsAiming = bEnabled;
}

void ATDSCharacter::MakeStun()
{
    bCanMoveCursore = false;
    GetCharacterMovement()->DisableMovement();
    if (!GetCurrentMontage())
    {
        PlayAnimMontage_Multicast(StunAnimation);
    }
}

void ATDSCharacter::ResetStun()
{
    bCanMoveCursore = true;
    GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
    StopAnimMontage_Multicast(StunAnimation);
}
