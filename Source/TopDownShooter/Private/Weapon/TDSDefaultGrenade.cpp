// This is educational project

#include "Weapon/TDSDefaultGrenade.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/DecalComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"
#include "Perception/AISense_Damage.h"

int32 DebugExplodeSphereShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TDS.DebugExplode"), DebugExplodeSphereShow, TEXT("Show debug explode spheres"), ECVF_Cheat);

ATDSDefaultGrenade::ATDSDefaultGrenade()
{
    bReplicates = true;
}

void ATDSDefaultGrenade::BeginPlay()
{
    Super::BeginPlay();

    check(GetWorld())

        GetWorld()
            ->GetTimerManager()
            .SetTimer(ExplotionTimer, this, &ATDSDefaultGrenade::Explode, ProjectileData.ExplosionCountdown, false);
}

void ATDSDefaultGrenade::ProjectileCollisionComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    Super::ProjectileCollisionComponentHit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
}

void ATDSDefaultGrenade::ImpactProjectile()
{
    Super::ImpactProjectile();
}

void ATDSDefaultGrenade::Explode()
{
    GetWorld()->GetTimerManager().ClearTimer(ExplotionTimer);

    if (DebugExplodeSphereShow)
    {
        DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileData.FullDamageRadius, 24, FColor::Red, false, 3.f, 0, 2.f);
        DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileData.DamageZoneRadius, 24, FColor::Blue, false, 3.f, 0, 2.f);
    }

    UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),                              //
                                                   ProjectileData.ProjectileDamage,         //
                                                   ProjectileData.ProjectileDamage * 0.2f,  //
                                                   GetActorLocation(),                      //
                                                   ProjectileData.FullDamageRadius,         //
                                                   ProjectileData.DamageZoneRadius,         //
                                                   5.f,                                     //
                                                   UDamageType::StaticClass(), {},          //
                                                   this,                                    //
                                                   nullptr);

    // UAISense_Damage::ReportDamageEvent(GetWorld(),                       //
    //                                    Hit.GetActor(),                   //
    //                                    GetOwner(),                       //
    //                                    ProjectileData.ProjectileDamage,  //
    //                                    Hit.TraceStart,                   //
    //                                    Hit.Location);

    // make trace to get FHitResult, use Hit.GetActor()

    SpawnExplosionFX_Multicast(ProjectileData.ExplosionEffect);
    SpawnExplosionSound_Multicast(ProjectileData.ExplosionSound);

    Destroy();
}

void ATDSDefaultGrenade::SpawnExplosionFX_Multicast_Implementation(UNiagaraSystem* ExplosionFX)
{
    UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ExplosionFX, GetActorLocation(), FRotator::ZeroRotator);
}

void ATDSDefaultGrenade::SpawnExplosionSound_Multicast_Implementation(USoundCue* Sound)
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
}
