// This is educational project

#include "Weapon/TDSDefaultProjectile.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/DecalComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"
#include "Perception/AISense_Damage.h"
#include "Net/UnrealNetwork.h"

ATDSDefaultProjectile::ATDSDefaultProjectile()
{
    PrimaryActorTick.bCanEverTick = false;
    bReplicates = true;

    CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
    CollisionComponent->SetSphereRadius(16.f);
    SetRootComponent(CollisionComponent);
    CollisionComponent->bReturnMaterialOnMove = true;      // hit Event returns PhysMaterial
    CollisionComponent->SetCanEverAffectNavigation(true);  // collision not affect navigation

    StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
    StaticMeshComponent->SetupAttachment(CollisionComponent);
    StaticMeshComponent->SetCanEverAffectNavigation(false);

    ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("UProjectileMovementComponent");
    ProjectileMovementComponent->UpdatedComponent = RootComponent;  //???
    ProjectileMovementComponent->InitialSpeed = 1.f;
    ProjectileMovementComponent->MaxSpeed = 1.f;  //???
    ProjectileMovementComponent->bRotationFollowsVelocity = true;
    ProjectileMovementComponent->bShouldBounce = true;
    ProjectileMovementComponent->ProjectileGravityScale = 0.f;

    ProjectileMovementComponent->SetIsReplicated(true);
    ProjectileMovementComponent->bInterpMovement = true;
}

void ATDSDefaultProjectile::BeginPlay()
{
    Super::BeginPlay();
    check(CollisionComponent);

    CollisionComponent->OnComponentHit.AddDynamic(this, &ATDSDefaultProjectile::ProjectileCollisionComponentHit);
    // CollisionComponent->OnComponentBeginOverlap.AddDynamic();
    // CollisionComponent->OnComponentEndOverlap.AddDynamic();
}

void ATDSDefaultProjectile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void ATDSDefaultProjectile::PostNetReceiveVelocity(const FVector& NewVelocity)
{
    // to set rotation to velocity
    //  ProjectileMovementComponent->Velocity = NewVelocity;
}

void ATDSDefaultProjectile::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

/*  Default Projectile spawns OnServer */
void ATDSDefaultProjectile::ProjectileCollisionComponentHit(UPrimitiveComponent* HitComponent,  //
                                                            AActor* OtherActor,                 //
                                                            UPrimitiveComponent* OtherComp,     //
                                                            FVector NormalImpulse,              //
                                                            const FHitResult& Hit)
{
    UpdateProjectileVelocity_Multicast(ProjectileMovementComponent->Velocity);
    FImpactData ImpactData;
    if (Hit.PhysMaterial.IsValid())
    {
        const auto PhysMat = Hit.PhysMaterial.Get();
        if (ImpactDataMap.Contains(PhysMat))
        {
            ImpactData = ImpactDataMap[PhysMat];
        }

        if (!ProjectileData.bIsGrenade)
        {
            UTypes::ApplyStateEffectByPhysicalMaterial(Hit.GetActor(),                   //
                                                       ProjectileData.StateEffectClass,  //
                                                       PhysMat,                          //
                                                       Hit.BoneName);
        }
    }

    if (ProjectileMovementComponent->Velocity.Size() > 350.f)
    {
        SpawnImpactFX_Multicast(ImpactData.ImpactEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
        SpawnImpactSound_Multicast(ImpactData.ImpactSound, Hit.ImpactPoint);
    }

    SpawnDecalAtLocation_Multicast(ImpactData.DecalMaterial,     //
                                   ImpactData.DecalSize,         //
                                   Hit.ImpactPoint,              //
                                   Hit.ImpactNormal.Rotation(),  //
                                   ImpactData.DecalLifeTime,     //
                                   ImpactData.DecalFadeOutTime);

    if (!ProjectileData.bIsGrenade)
    {

        UGameplayStatics::ApplyPointDamage(Hit.GetActor(),                   //
                                           ProjectileData.ProjectileDamage,  //
                                           Hit.TraceStart,                   //
                                           Hit, GetInstigatorController(),   //
                                           GetOwner(),                       //
                                           nullptr);
        UAISense_Damage::ReportDamageEvent(GetWorld(),                       //
                                           Hit.GetActor(),                   //
                                           GetOwner(),                       //
                                           ProjectileData.ProjectileDamage,  //
                                           Hit.TraceStart,                   //
                                           Hit.Location);
        ImpactProjectile();
    }
}

void ATDSDefaultProjectile::InitProjectile(const FProjectileData& Data)
{
    /* handled on server
     * check same condition if used in blueprints */
    ProjectileData = Data;

    if (!Data.bIsGrenade)
    {
        SetLifeSpan(Data.ProjectileLifetime);
    }
    if (Data.bIsGrenade)
    {
        ProjectileMovementComponent->ProjectileGravityScale = 1.0f;
    }

    ProjectileData.ProjectileMesh                                                                             //
        ?                                                                                                     //
        SetProjectileMeshAndTransform_Multicast(ProjectileData.ProjectileMesh, ProjectileData.MeshTransform)  //
        :                                                                                                     //
        StaticMeshComponent->DestroyComponent();

    UpdateProjectileSpeedAndVelocity_Multicast(Data.ProjectileInitialSpeed, ShootDirection);

    SpawnTraceFX_Multicast(ProjectileData.TraceFX);
}

void ATDSDefaultProjectile::InitProjectileImpactEffects(const TMap<UPhysicalMaterial*, FImpactData>& Data)
{
    ImpactDataMap = Data;
}

void ATDSDefaultProjectile::ImpactProjectile()
{
    Destroy();
}

void ATDSDefaultProjectile::UpdateProjectileVelocity_Multicast_Implementation(const FVector& Velocity)
{
    if (HasAuthority()) return;
    ProjectileMovementComponent->Velocity = Velocity;
    ProjectileMovementComponent->UpdateComponentVelocity();
}

void ATDSDefaultProjectile::SetProjectileMeshAndTransform_Multicast_Implementation(UStaticMesh* Mesh, FTransform LocalTransform)
{
    StaticMeshComponent->SetStaticMesh(Mesh);
    StaticMeshComponent->AddLocalTransform(LocalTransform);
}

void ATDSDefaultProjectile::UpdateProjectileSpeedAndVelocity_Multicast_Implementation(float Speed, const FVector& Direction)
{
    ProjectileMovementComponent->InitialSpeed = Speed;
    ProjectileMovementComponent->MaxSpeed = Speed;
    // if (GetRemoteRole() == ENetRole::ROLE_SimulatedProxy) return;
    if (HasAuthority()) return;
    ProjectileMovementComponent->Velocity = Speed * Direction;
    ProjectileMovementComponent->UpdateComponentVelocity();
}

void ATDSDefaultProjectile::SpawnTraceFX_Multicast_Implementation(UNiagaraSystem* TraceFX)
{
    FRotator Rotation = FRotator(180.f, 0.f, 0.f);
    UNiagaraFunctionLibrary::SpawnSystemAttached(TraceFX, StaticMeshComponent, FName(), FVector::ZeroVector, /*FRotator::ZeroRotator*/ Rotation, EAttachLocation::SnapToTarget, true);
}

void ATDSDefaultProjectile::SpawnDecalAtLocation_Multicast_Implementation(
    UMaterialInterface* Material, FVector Size, const FVector& ImpactPoint, const FRotator& Rotation, float DecalLifetime, float DecalFadeOutTime)
{
    auto Decal = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),   //
                                                        Material,     //
                                                        Size,         //
                                                        ImpactPoint,  //
                                                        Rotation);

    if (Decal)
    {
        Decal->SetFadeOut(DecalLifetime, DecalFadeOutTime);
    }
}

void ATDSDefaultProjectile::SpawnImpactFX_Multicast_Implementation(UNiagaraSystem* ImpactFX, const FVector& ImpactPoint, const FRotator& Rotation)
{
    UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, ImpactFX, ImpactPoint, Rotation);
}

void ATDSDefaultProjectile::SpawnImpactSound_Multicast_Implementation(USoundCue* Sound, const FVector& ImpactPoint)
{
    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, ImpactPoint);
}
