// This is educational project

#include "Weapon/TDSDefaultWeapon.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/DecalComponent.h"
#include "Weapon/TDSDefaultProjectile.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "NiagaraFunctionLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"
#include "NiagaraComponent.h"
#include "Buff/TDSStateEffect.h"
#include "FuncLibrary/Types.h"
#include "Perception/AISense_Damage.h"
#include "Net/UnrealNetwork.h"

int32 DebugWeaponSpreadConeShow = 0;
FAutoConsoleVariableRef ConsoleVariab(TEXT("TDS.DebugCone"), DebugWeaponSpreadConeShow, TEXT("Show weapon spread debug cone"), ECVF_Cheat);

DEFINE_LOG_CATEGORY_STATIC(TDSDefaultWeapon_LOG, All, All);

ATDSDefaultWeapon::ATDSDefaultWeapon()
{
    bReplicates = true;
    PrimaryActorTick.bCanEverTick = true;

    SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
    SetRootComponent(SceneComponent);

    SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMeshComponent");
    SkeletalMeshComponent->SetGenerateOverlapEvents(false);
    SkeletalMeshComponent->SetCollisionProfileName("NoCollision");
    SkeletalMeshComponent->SetupAttachment(SceneComponent);

    StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
    StaticMeshComponent->SetGenerateOverlapEvents(false);
    StaticMeshComponent->SetCollisionProfileName("NoCollision");
    StaticMeshComponent->SetupAttachment(SceneComponent);

    MuzzleLocationComp = CreateDefaultSubobject<UArrowComponent>("ShootLocation");
    MuzzleLocationComp->SetupAttachment(SceneComponent);

    SleevEjectLocation = CreateDefaultSubobject<UArrowComponent>("SleevEjectLocation");
    SleevEjectLocation->SetupAttachment(SceneComponent);
    SleevEjectLocation->ArrowColor = FColor::Purple;

    ClipEjectLocation = CreateDefaultSubobject<UArrowComponent>("ClipEjectLocation");
    ClipEjectLocation->SetupAttachment(SceneComponent);
    ClipEjectLocation->ArrowColor = FColor::Cyan;
}

void ATDSDefaultWeapon::BeginPlay()
{
    Super::BeginPlay();

    check(GetWorld());

    InitWeapon();
}

void ATDSDefaultWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSDefaultWeapon, AdditionalWeaponData);
    DOREPLIFETIME(ATDSDefaultWeapon, bReloadInProgress);
}

void ATDSDefaultWeapon::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    GetWorld()->GetTimerManager().ClearTimer(ReloadingTimer);
    GetWorld()->GetTimerManager().ClearTimer(AutomaticFireTimer);
    GetWorld()->GetTimerManager().ClearTimer(ChangingSpreadTimer);

    Super::EndPlay(EndPlayReason);
}

void ATDSDefaultWeapon::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    // Debug visualization
    if (DebugWeaponSpreadConeShow)
    {
        const auto OwnerDirection = GetOwner() ? GetOwner()->GetActorForwardVector() : FVector::ZeroVector;

        DrawDebugCone(GetWorld(),                                  //
                      MuzzleLocationComp->GetComponentLocation(),  //
                      OwnerDirection,                              //
                      2000.f,                                      //
                      CurrentWeaponSpread,                         //
                      CurrentWeaponSpread,                         //
                      32,                                          //
                      FColor::Cyan,                                //
                      false,                                       //
                      0);
    }
}

void ATDSDefaultWeapon::SetWeaponStartFire_OnServer_Implementation(bool bIsFiring)
{
    bIsPlayerFiring = bIsFiring;

    if (!CanFire()) return;

    if (bIsFiring)
    {
        MakeShot();
    }

    if (!GetWorld()->GetTimerManager().IsTimerActive(AutomaticFireTimer) && bIsFiring)
    {
        GetWorld()->GetTimerManager().SetTimer(AutomaticFireTimer,                       //
                                               this,                                     //
                                               &ATDSDefaultWeapon::LoadCartrigeAndShot,  //
                                               WeaponData.RateOfFire,                    //
                                               true);
    }
}

void ATDSDefaultWeapon::SetWeaponData(const FWeaponData& Data)
{
    WeaponData = Data;

    // ======================================================???
    CurrentWeaponSpread = Data.WeaponSpread.MinSpread;
}

void ATDSDefaultWeapon::UpdateStateWeapon_OnServer_Implementation(EMovementState MovementState)
{
    switch (MovementState)
    {
        case EMovementState::Aim_State:
            MinSpreadInState = WeaponData.WeaponSpread.MinSpreadAiming;
            MaxSpreadInState = WeaponData.WeaponSpread.MaxSpreadAiming;
            break;
        case EMovementState::AimWalk_State:
            MinSpreadInState = WeaponData.WeaponSpread.MinSpreadAiming;
            MaxSpreadInState = WeaponData.WeaponSpread.MaxSpreadAiming;
            break;
        default:
            MinSpreadInState = WeaponData.WeaponSpread.MinSpread;
            MaxSpreadInState = WeaponData.WeaponSpread.MaxSpread;
            break;
    }
}

void ATDSDefaultWeapon::UpdateWeaponSpread()
{
    // New logic, to get back Modifier = 1 : -1
    const int8 Modifier = GetWorld()->GetTimerManager().IsTimerActive(AutomaticFireTimer) ? 0 : -1;
    // If New logic - remove Timer with delegate, set timer with reduce func
    FTimerDelegate SpreadDelegate;
    SpreadDelegate.BindUFunction(this, "ChangeSpread", Modifier);
    GetWorld()->GetTimerManager().SetTimer(ChangingSpreadTimer, SpreadDelegate, 0.1, true);
}

void ATDSDefaultWeapon::ReloadWeapon_OnServer_Implementation()
{
    if (bReloadInProgress || AdditionalWeaponData.AmmoInWeapon == WeaponData.MaxAmmo) return;

    bReloadInProgress = true;
    UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("Start reload"));

    GetWorld()->GetTimerManager().SetTimer(ReloadingTimer,                      //
                                           this,                                //
                                           &ATDSDefaultWeapon::ReloadFinished,  //
                                           WeaponData.ReloadTime,               //
                                           false);

    GetWorld()->GetTimerManager().SetTimer(ClipEjectTimer, this, &ATDSDefaultWeapon::EjectClip, WeaponData.ClipEjectDelay, false);

    if (!WeaponData.ReloadingAnimation) return;
    OnReloadStart.Broadcast(WeaponData.ReloadingAnimation);
}

void ATDSDefaultWeapon::CancelReloading()
{
    if (!bReloadInProgress) return;

    if (SkeletalMeshComponent && SkeletalMeshComponent->GetAnimInstance())
    {
        SkeletalMeshComponent->GetAnimInstance()->StopAllMontages(0.15f);
    }

    bReloadInProgress = false;
    GetWorld()->GetTimerManager().ClearTimer(ReloadingTimer);
    OnReloadEnd.Broadcast(false, AdditionalWeaponData.AmmoInWeapon);
}

/* used in inventory widget */
float ATDSDefaultWeapon::ReturnRemainingReloadTime() const
{
    if (!GetWorld()->GetTimerManager().IsTimerActive(ReloadingTimer)) return WeaponData.ReloadTime;

    return GetWorld()->GetTimerManager().GetTimerRemaining(ReloadingTimer);
}
//  function for debug user widget
float ATDSDefaultWeapon::ReturnRemainingLoadCartrigeTime() const
{
    if (!GetWorld()->GetTimerManager().IsTimerActive(AutomaticFireTimer)) return WeaponData.RateOfFire;

    return GetWorld()->GetTimerManager().GetTimerRemaining(AutomaticFireTimer);
}

/* keep current wepon mesh to spawn weapon pickup */
UMeshComponent* ATDSDefaultWeapon::GetWeaponMesh() const
{
    FString TestText = StaticMeshComponent ? "Enable StaticMesh Component" : "No StaticMesh Component";
    UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("%s %s"), *this->GetName(), *TestText);

    FString TestText2 = SkeletalMeshComponent ? "Enable SkeletalMesh Component" : "No SkeletalMesh Component";
    UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("%s %s"), *this->GetName(), *TestText2);

    if (StaticMeshComponent && StaticMeshComponent->GetStaticMesh())
    {
        return StaticMeshComponent;
    }
    else if (SkeletalMeshComponent && SkeletalMeshComponent->SkeletalMesh)
    {
        return SkeletalMeshComponent;
    }
    return nullptr;
}

void ATDSDefaultWeapon::InitWeapon()
{
    if (StaticMeshComponent && !StaticMeshComponent->GetStaticMesh())
    {
        StaticMeshComponent->DestroyComponent(true);
    }
    if (SkeletalMeshComponent && !SkeletalMeshComponent->SkeletalMesh)
    {
        SkeletalMeshComponent->DestroyComponent(true);
    }
}

void ATDSDefaultWeapon::MakeShot()
{
    /* handled on server */
    if (!bIsCartrigeLoaded) return;

    CreateFXWeaponEffect();

    const int8 ProjectilesNum = WeaponData.bIsScuttergun ? (FMath::RandHelper(6) + 6) : 1;

    for (int8 i = 0; i < ProjectilesNum; ++i)
    {
        WeaponData.bIsSpawnProjectile ? SpawnBullet() : MakeHitScan();
    }

    /* only when sooting increase spread */
    const float NewSpread = WeaponData.WeaponSpread.SpreadModifier * CurrentWeaponSpread;
    CurrentWeaponSpread = FMath::Clamp(NewSpread, MinSpreadInState, MaxSpreadInState);

    ReduceAmmo();

    /* update inventory widget, play character's animation */
    OnWeaponMakeShot.Broadcast(WeaponData.ShotType);

    bIsCartrigeLoaded = false;
}

void ATDSDefaultWeapon::SpawnBullet()
{
    if (!MuzzleLocationComp) return;

    const FTransform SpawnTransform(MuzzleLocationComp->GetComponentRotation(), MuzzleLocationComp->GetComponentLocation());

    auto Bullet = GetWorld()->SpawnActorDeferred<ATDSDefaultProjectile>(WeaponData.ProjectileConfig.ProjectileClass, SpawnTransform);
    // auto Bullet = GetWorld()->SpawnActor<ATDSDefaultProjectile>(WeaponData.ProjectileConfig.ProjectileClass, SpawnTransform);

    const auto OwnerDirection = GetOwner() ? GetOwner()->GetActorForwardVector() : FVector::ZeroVector;
    const FVector ShootDirection = FMath::VRandCone(OwnerDirection, CurrentWeaponSpread);

    if (Bullet)
    {
        Bullet->SetShootDirection(ShootDirection);
        Bullet->InitProjectile(WeaponData.ProjectileConfig);
        Bullet->InitProjectileImpactEffects(WeaponData.ImpactDataMap);
        Bullet->FinishSpawning(SpawnTransform);
        Bullet->SetOwner(GetOwner());
    }
}

void ATDSDefaultWeapon::ReduceAmmo()
{
    /* handled on server */
    --AdditionalWeaponData.AmmoInWeapon;
    UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("Ammo: %i"), AdditionalWeaponData.AmmoInWeapon);

    if (AdditionalWeaponData.AmmoInWeapon == 0)
    {
        UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("Magazine is empty"));

        GetWorld()->GetTimerManager().PauseTimer(AutomaticFireTimer);

        ReloadWeapon_OnServer();
    }
}

void ATDSDefaultWeapon::CreateHitScanFXImpactEffect(const FHitResult& Hit)
{
    FImpactData ImpactData;

    if (Hit.PhysMaterial.IsValid())
    {
        const auto PhysMat = Hit.PhysMaterial.Get();
        if (WeaponData.ImpactDataMap.Contains(PhysMat))
        {
            ImpactData = WeaponData.ImpactDataMap[PhysMat];
        }
    }

    SpawnImpactFX_Multicast(ImpactData.ImpactEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
    SpawnDecalAtLocation_Multicast(ImpactData.DecalMaterial, ImpactData.DecalSize, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), ImpactData.DecalLifeTime, ImpactData.DecalFadeOutTime);
    SpawnImpactSound_Multicast(ImpactData.ImpactSound, Hit.ImpactPoint);
}

void ATDSDefaultWeapon::SpawnImpactFX_Multicast_Implementation(UNiagaraSystem* ImpactFX, const FVector& ImpactPoint, const FRotator& Rotation)
{
    UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ImpactFX, ImpactPoint, Rotation);
}

void ATDSDefaultWeapon::SpawnDecalAtLocation_Multicast_Implementation(
    UMaterialInterface* Material, FVector Size, const FVector& ImpactPoint, const FRotator& Rotation, float DecalLifetime, float DecalFadeOutTime)
{
    auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), Material, Size, ImpactPoint, Rotation);
    if (!DecalComponent) return;

    DecalComponent->SetFadeOut(DecalLifetime, DecalFadeOutTime);
}

void ATDSDefaultWeapon::SpawnImpactSound_Multicast_Implementation(USoundCue* Sound, const FVector& ImpactPoint)
{
    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, ImpactPoint);
}

void ATDSDefaultWeapon::CreateFXWeaponEffect()
{
    SpawnMazzleFX_Multicast(WeaponData.MuzzleEffect, WeaponData.MuzzleTransform);

    if (SleevEjectLocation)
    {
        SpawnSleevEjectFX_Multicast(WeaponData.SleevEjectEffect);
    }

    if (MuzzleLocationComp)
    {
        SpawnShootingSound_Multicast(WeaponData.ShootingSound);
    }
}

void ATDSDefaultWeapon::SpawnMazzleFX_Multicast_Implementation(UNiagaraSystem* MuzzleFX, const FTransform& MuzzleTransform)
{
    UNiagaraFunctionLibrary::SpawnSystemAttached(MuzzleFX,                       //
                                                 MuzzleLocationComp,             //
                                                 NAME_None,                      //
                                                 MuzzleTransform.GetLocation(),  //
                                                 MuzzleTransform.Rotator(),      //
                                                 EAttachLocation::SnapToTarget,  //
                                                 true);
}

void ATDSDefaultWeapon::SpawnSleevEjectFX_Multicast_Implementation(UNiagaraSystem* SleevEjectFX)
{
    UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),                                  //
                                                   SleevEjectFX,                                //
                                                   SleevEjectLocation->GetComponentLocation(),  //
                                                   SleevEjectLocation->GetComponentRotation());
}

void ATDSDefaultWeapon::SpawnShootingSound_Multicast_Implementation(USoundCue* Sound)
{
    UGameplayStatics::SpawnSoundAtLocation(GetWorld(),  //
                                           Sound,       //
                                           MuzzleLocationComp->GetComponentLocation());
}

void ATDSDefaultWeapon::LoadCartrigeAndShot()
{
    /* handled on server */
    bIsCartrigeLoaded = true;

    if (bIsPlayerFiring && CanFire())
    {
        MakeShot();
    }
    else
    {
        GetWorld()->GetTimerManager().ClearTimer(AutomaticFireTimer);
    }

    UpdateWeaponSpread();
}

void ATDSDefaultWeapon::MakeHitScan()
{
    if (!MuzzleLocationComp) return;

    const auto TraceStart = MuzzleLocationComp->GetComponentLocation();
    const auto OwnerDirection = GetOwner() ? GetOwner()->GetActorForwardVector() : FVector::ZeroVector;
    const auto ShootDirection = FMath::VRandCone(OwnerDirection, CurrentWeaponSpread);
    FVector TraceEnd = TraceStart + ShootDirection * WeaponData.DistanceTrace;

    FHitResult HitResult;

    FCollisionQueryParams QuerryParams;
    QuerryParams.AddIgnoredActor(GetOwner());
    QuerryParams.bReturnPhysicalMaterial = true;
    GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, QuerryParams);

    if (HitResult.bBlockingHit)
    {
        TraceEnd = HitResult.ImpactPoint;

        CreateHitScanFXImpactEffect(HitResult);

        DealDamage(HitResult);

        if (HitResult.PhysMaterial.IsValid())
        {
            const auto PhysMat = HitResult.PhysMaterial.Get();

            UTypes::ApplyStateEffectByPhysicalMaterial(HitResult.GetActor(),                          //
                                                       WeaponData.ProjectileConfig.StateEffectClass,  //
                                                       PhysMat,                                       //
                                                       HitResult.BoneName);
        }
        // UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("Hit: %s"), *HitResult.BoneName.ToString());
    }

    if (MuzzleLocationComp)
    {
        SpawnTraceFX_Multicast(WeaponData.TraceEffect, TraceEnd);
    }
}

void ATDSDefaultWeapon::SpawnTraceFX_Multicast_Implementation(UNiagaraSystem* TraceFX, const FVector& TraceEnd)
{
    const auto TraceComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, MuzzleLocationComp->GetComponentLocation(), MuzzleLocationComp->GetComponentRotation());
    TraceComponent->SetVariableVec3("TraceEnd", TraceEnd);
}

void ATDSDefaultWeapon::DealDamage(const FHitResult& HitResult)
{
    UGameplayStatics::ApplyPointDamage(HitResult.GetActor(),       //
                                       WeaponData.WeaponDamage,    //
                                       HitResult.TraceStart,       //
                                       HitResult,                  //
                                       GetInstigatorController(),  //
                                       GetOwner(),                 //
                                       nullptr);

    UAISense_Damage::ReportDamageEvent(GetWorld(), HitResult.GetActor(), GetOwner(), WeaponData.WeaponDamage, HitResult.TraceStart, HitResult.Location);
}

bool ATDSDefaultWeapon::CanFire()
{
    if (bReloadInProgress || AdditionalWeaponData.AmmoInWeapon <= 0) return false;

    return true;
}

void ATDSDefaultWeapon::ReloadFinished()
{
    bReloadInProgress = false;
    bIsCartrigeLoaded = true;

    GetWorld()->GetTimerManager().UnPauseTimer(AutomaticFireTimer);
    GetWorld()->GetTimerManager().ClearTimer(ReloadingTimer);

    OnReloadEnd.Broadcast(true, AdditionalWeaponData.AmmoInWeapon);  // broadcast reference to rewrite ammo
    UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("Reload ends"));
}

void ATDSDefaultWeapon::ChangeSpread(int8 Modifier)
{
    const float NewSpread = CurrentWeaponSpread + Modifier * (WeaponData.WeaponSpread.SpreadModifier - 1) * CurrentWeaponSpread;
    CurrentWeaponSpread = FMath::Clamp(NewSpread, MinSpreadInState, MaxSpreadInState);

    if (FMath::IsNearlyEqual(CurrentWeaponSpread, MinSpreadInState, 0.01f))
    {
        UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("Stop ChangingSpreadTimer"));
        GetWorld()->GetTimerManager().ClearTimer(ChangingSpreadTimer);
        return;
    }
    if (FMath::IsNearlyEqual(CurrentWeaponSpread, MaxSpreadInState, 0.01f))
    {
        UE_LOG(TDSDefaultWeapon_LOG, Display, TEXT("Stop ChangingSpreadTimer"));
        GetWorld()->GetTimerManager().ClearTimer(ChangingSpreadTimer);
        return;
    }
}

void ATDSDefaultWeapon::EjectClip()
{
    if (!ClipEjectLocation) return;

    SpawnClipEjectFX_Multicast(WeaponData.ClipEjectEffect);

    GetWorld()->GetTimerManager().ClearTimer(ClipEjectTimer);
}

void ATDSDefaultWeapon::SpawnClipEjectFX_Multicast_Implementation(UNiagaraSystem* ClipEjectFX)
{
    UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),                                 //
                                                   ClipEjectFX,                                //
                                                   ClipEjectLocation->GetComponentLocation(),  //
                                                   ClipEjectLocation->GetComponentRotation());
}
