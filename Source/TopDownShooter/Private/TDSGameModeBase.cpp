// This is educational project

#include "TDSGameModeBase.h"
#include "Player/TDSCharacter.h"
#include "Player/TDSPlayerController.h"
#include "SupportingFiles/TDSSpawningActor.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TDSHealthComponent.h"
#include "Components/TDSRespawnComponent.h"
#include "Components/TDSScoreComponent.h"
#include "Player/TDSPlayerState.h"
#include "Interfaces/TDSActorInterface.h"
#include "AIController.h"
#include "TDSGameInstance.h"
#include "EngineUtils.h"

DEFINE_LOG_CATEGORY_STATIC(GameModeBase_LOG, All, All);

ATDSGameModeBase::ATDSGameModeBase()
{
    DefaultPawnClass = ATDSCharacter::StaticClass();
    PlayerControllerClass = ATDSPlayerController::StaticClass();
    PlayerStateClass = ATDSPlayerState::StaticClass();
}

void ATDSGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
    Super::InitGame(MapName, Options, ErrorMessage);

    FillSpawningActorsMap();
    SetupGamePhaseDataArray();

    BeginNewPhase();
}

void ATDSGameModeBase::StartPlay()
{
    Super::StartPlay();
}

UClass* ATDSGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
    if (InController && InController->IsA<AAIController>())
    {
        const auto RandomIndex = FMath::RandHelper(AIPawnClasses.Num());
        if (AIPawnClasses.IsValidIndex(RandomIndex))
        {
            return AIPawnClasses[RandomIndex];
        }
    }
    return Super::GetDefaultPawnClassForController_Implementation(InController);
}

AActor* ATDSGameModeBase::ChoosePlayerStart_Implementation(AController* Player)
{
    if (Player && Player->IsA<APlayerController>() && !CurrentGamePhaseData.PhaseName.IsNone())
    {
        const auto PhaseName = CurrentGamePhaseData.PhaseName.ToString();
        return FindPlayerStart(Player, PhaseName);
    }

    return Super::ChoosePlayerStart_Implementation(Player);
}

void ATDSGameModeBase::Killed(AController* VictimController, AController* KillerController)
{
    const auto ScoreComponent = VictimController ? VictimController->FindComponentByClass<UTDSScoreComponent>() : nullptr;
    const int32 PointsToAdd = ScoreComponent ? ScoreComponent->GetScorePoints() : 0;

    if (!CanVictimPlayerRespawn(VictimController)) return;

    UpdateKillerPlayerStatus(KillerController, PointsToAdd);

    if (PointsToAdd)
    {
        GameProgressPoints += PointsToAdd;
        OnProgressPointsChanged.Broadcast(GameProgressPoints);
    }

    UE_LOG(GameModeBase_LOG, Display, TEXT("ScorePoints: %i"), GameProgressPoints);

    if (VictimController && GameProgressPoints < CurrentGamePhaseData.ScoreToRichNextPhase)
    {
        StartRespawning(VictimController);
    }

    if (GameProgressPoints >= CurrentGamePhaseData.ScoreToRichNextPhase)
    {
        ++PhaseIndex;

        ResetScorePointsAllAliveEnemies();

        BeginNewPhase();
    }
}

void ATDSGameModeBase::TryToRespawn(AController* Controller)
{
    if (Controller                           //
        && Controller->IsA<AAIController>()  //
        && CurrentGamePhaseData.EnabledEnemiesNumber < EnemyControllers.Num())
    {
        int32 ControllerIndex{-1};
        if (EnemyControllers.Find(Cast<AAIController>(Controller), ControllerIndex))
        // if (EnemyControllers.IsValidIndex(ControllerIndex))
        {
            EnemyControllers.RemoveAt(ControllerIndex);
            Controller->Destroy();
            return;
        }
    }

    ResetPawn(CurrentGamePhaseData.PhaseName, Controller);
}

bool ATDSGameModeBase::CanVictimPlayerRespawn(AController* VictimController)
{
    const auto PC = Cast<ATDSPlayerController>(VictimController);
    const auto VictimPlayerState = PC ? Cast<ATDSPlayerState>(PC->PlayerState) : nullptr;
    if (!VictimPlayerState) return true;
    VictimPlayerState->DecreaseLive();
    UE_LOG(GameModeBase_LOG, Display, TEXT("Lives: %i"), VictimPlayerState->GetLives());
    // PC->StartSpectating();
    if (VictimPlayerState->GetLives()) return true;

    return false;
}

void ATDSGameModeBase::UpdateKillerPlayerStatus(AController* KillerController, int32 Points)
{
    const auto KillerPlayerState = KillerController ? Cast<ATDSPlayerState>(KillerController->PlayerState) : nullptr;

    if (!KillerPlayerState) return;
    KillerPlayerState->AddScorePoints(Points);
}

void ATDSGameModeBase::SetupGamePhaseDataArray()
{
    if (!GetWorld() || !GetWorld()->GetGameInstance()) return;

    const auto TDSGameInstance = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
    if (!TDSGameInstance) return;

    const auto CurrentLevelName = FName(*UGameplayStatics::GetCurrentLevelName(GetWorld()));
    FLevelData LevelData;
    if (!TDSGameInstance->GetLevelDataByName(CurrentLevelName, LevelData))
    {
        UE_LOG(GameModeBase_LOG, Display, TEXT("Couldn't find any level data"));
        return;
    }

    GamePhaseDataArray = LevelData.GamePhases;
}

void ATDSGameModeBase::FillSpawningActorsMap()
{
    for (const auto& FoundActor : TActorRange<ATDSSpawningActor>(GetWorld()))
    {
        const auto FoundSpawningActor = Cast<ATDSSpawningActor>(FoundActor);
        if (!FoundSpawningActor) continue;

        SpawningActorsMap.FindOrAdd(FoundSpawningActor->GetPhaseName()).Add(FoundSpawningActor);
    }

    // debug
    // for (TPair<FName, TArray<ATDSSpawningActor*>>& Element : SpawningActorsMap)
    //{
    //    UE_LOG(GameModeBase_LOG, Display, TEXT("Game Phase: %s"), *Element.Key.ToString());
    //    for (const auto& FoundActor : Element.Value)
    //    {
    //        UE_LOG(GameModeBase_LOG, Display, TEXT("%s"), *FoundActor->GetName());
    //    }
    //}
}

void ATDSGameModeBase::StartRespawning(AController* Controller)
{
    const auto RespawnComponent = Controller->FindComponentByClass<UTDSRespawnComponent>();
    if (RespawnComponent)
    {
        RespawnComponent->StartRespawning(3.f);  //
    }
}

void ATDSGameModeBase::ResetPawn(const FName& PhaseName, AController* PawnController)
{
    if (!PawnController) return;
    PawnController->UnPossess();

    if (PawnController && PawnController->IsA<APlayerController>())
    {
        const auto StartSpot = FindPlayerStart(PawnController, PhaseName.ToString());
        if (StartSpot)
        {
            RestartPlayerAtPlayerStart(PawnController, StartSpot);
            return;
        }
    }

    const auto SpawningActorsArrayPointer = SpawningActorsMap.Find(PhaseName);
    if (!SpawningActorsArrayPointer)
    {
        UE_LOG(GameModeBase_LOG, Display, TEXT("Couldn't find correct spawner"));
        return;
    }

    const auto SpawningActorsArray = *SpawningActorsArrayPointer;
    const auto RandomIndex = FMath::RandHelper(SpawningActorsArray.Num());
    const auto StartSpot = SpawningActorsArray[RandomIndex];
    RestartPlayerAtPlayerStart(PawnController, StartSpot);
}

void ATDSGameModeBase::ResetScorePointsAllAliveEnemies()
{
    UE_LOG(GameModeBase_LOG, Display, TEXT("Set zero score alive enemies"));
    for (const auto EnemyController : EnemyControllers)
    {
        if (!EnemyController->GetPawn()) continue;

        const auto ScoreComponent = EnemyController->FindComponentByClass<UTDSScoreComponent>();
        if (!ScoreComponent) continue;
        ScoreComponent->ResetScorePoints();
    }
}

void ATDSGameModeBase::BeginNewPhase()
{
    // ResetScorePointsAllAliveEnemies();

    if (!GamePhaseDataArray.IsValidIndex(PhaseIndex)) return;
    CurrentGamePhaseData = GamePhaseDataArray[PhaseIndex];

    GameProgressPoints = 0;
    OnProgressPointsChanged.Broadcast(GameProgressPoints);
    OnPhasePointsChanged.Broadcast(CurrentGamePhaseData.ScoreToRichNextPhase);
    OnGamePhaseChanged.Broadcast(CurrentGamePhaseData.PhaseName);

    auto EnemiesNeeded = CurrentGamePhaseData.EnabledEnemiesNumber - EnemyControllers.Num();
    if (EnemiesNeeded > 0)
    {
        FActorSpawnParameters SpawnInfo;
        SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

        while (EnemiesNeeded > 0)
        {
            const auto EnemyController = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnInfo);
            EnemyControllers.Add(EnemyController);
            ResetPawn(CurrentGamePhaseData.PhaseName, EnemyController);
            --EnemiesNeeded;
        }
    }

    if (CurrentGamePhaseData.PhaseBossClass)
    {
        SpawnBoss();
    }
}

void ATDSGameModeBase::SpawnBoss()
{
    if (!GetWorld()) return;

    FActorSpawnParameters SpawnParams;
    SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

    const auto SpawningActorsArrayPointer = SpawningActorsMap.Find(CurrentGamePhaseData.PhaseName);
    if (!SpawningActorsArrayPointer) return;
    const auto RandomIndex = FMath::RandHelper((*SpawningActorsArrayPointer).Num());
    const auto StartSpot = (*SpawningActorsArrayPointer)[RandomIndex];
    if (!StartSpot) return;
    const auto NewPawn = GetWorld()->SpawnActor<APawn>(CurrentGamePhaseData.PhaseBossClass, StartSpot->GetActorLocation(), StartSpot->GetActorRotation(), SpawnParams);

    if (!NewPawn) return;

    if (!NewPawn->Controller)
    {
        NewPawn->SpawnDefaultController();
        EnemyControllers.Add(Cast<AAIController>(NewPawn->Controller));
    }
}
