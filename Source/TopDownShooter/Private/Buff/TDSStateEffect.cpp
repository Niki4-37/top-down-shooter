// This is educational project

#include "Buff/TDSStateEffect.h"
#include "Components/TDSHealthComponent.h"
#include "NiagaraFunctionLibrary.h"  //!!! delete
#include "NiagaraComponent.h"
#include "Interfaces/TDSActorInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(UTDSStateEffect_LOG, All, All);

/* calls from UTypes::ApplyStateEffectByPhysicalMaterial */
void UTDSStateEffect::InitObject(AActor* ActorToApply, FName BoneName)
{
    ActorToApplyEffect = ActorToApply;
    BoneToApplyEffectName = BoneName;

    const auto HealthComponent = ActorToApply->FindComponentByClass<UTDSHealthComponent>();
    bIsApplied = HealthComponent ? HealthComponent->TryToApplyStateEffect(this) : false;
    // HealthComponent ? HealthComponent->TryToApplyStateEffect_OnServer(this) : DestroyObject();

    if (!bIsApplied)
    {
        DestroyObject();
        UE_LOG(UTDSStateEffect_LOG, Display, TEXT("Not Applied!"));
    }
}

void UTDSStateEffect::DestroyObject()
{
    /* this checking delete state effect from array*/
    if (ActorToApplyEffect && bIsApplied)
    {
        if (const auto HealthComponent = ActorToApplyEffect->FindComponentByClass<UTDSHealthComponent>())
        {
            HealthComponent->RemoveStateEffect(this);
        }
    }

    ActorToApplyEffect = nullptr;

    if (IsValidLowLevel())
    {
        ConditionalBeginDestroy();
    }
}

void UTDSStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSStateEffect, BoneToApplyEffectName);
}

//===============================================================
void UTSDStateEffectExecuteOnce::InitObject(AActor* ActorToApply, FName BoneName)
{
    Super::InitObject(ActorToApply, BoneName);
    ExecuteOnce();
}

void UTSDStateEffectExecuteOnce::DestroyObject()
{
    Super::DestroyObject();
}

void UTSDStateEffectExecuteOnce::ExecuteOnce()
{
    if (ActorToApplyEffect && bIsApplied)
    {
        if (const auto HealthComponent = ActorToApplyEffect->FindComponentByClass<UTDSHealthComponent>())
        {
            HealthComponent->TryToAddHealth(AmountValue);
        }
    }

    DestroyObject();
}
//===============================================================
void UTSDStateEffecExecuteInTime::InitObject(AActor* ActorToApply, FName BoneName)
{
    Super::InitObject(ActorToApply, BoneName);

    Execute();

    if (GetWorld() && ActorToApply && bIsApplied)
    {
        if (const auto HealthComponent = ActorToApply->FindComponentByClass<UTDSHealthComponent>())
        {
            if (bIsBoost)
            {
                HealthComponent->BoostHealth(BoostFactor);
            }
            if (bIsInvulnerable)
            {
                HealthComponent->SetInvulnarable(true);
            }
        }

        GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &UTSDStateEffecExecuteInTime::DestroyObject, EffectDuration, false);
        GetWorld()->GetTimerManager().SetTimer(ExecutionTimer, this, &UTSDStateEffecExecuteInTime::Execute, ExecutionRate, bIsChangeHealth);
    }
}

void UTSDStateEffecExecuteInTime::DestroyObject()
{
    const auto HealthComponent = ActorToApplyEffect->FindComponentByClass<UTDSHealthComponent>();
    if (HealthComponent && bIsApplied)
    {
        if (bIsBoost)
        {
            HealthComponent->BoostHealth(1 / BoostFactor);
        }
        else if (bIsInvulnerable)
        {
            HealthComponent->SetInvulnarable(false);
        }
    }

    if (GetWorld())
    {
        GetWorld()->GetTimerManager().ClearTimer(DestroyTimer);
        GetWorld()->GetTimerManager().ClearTimer(ExecutionTimer);
    }

    const auto ActorWithInterface = Cast<ITDSActorInterface>(ActorToApplyEffect);
    if (ActorWithInterface && bCanStun && bIsApplied)
    {
        ActorWithInterface->ResetStun();
    }

    Super::DestroyObject();
}

void UTSDStateEffecExecuteInTime::Execute()
{
    // UE_LOG(LogTemp, Display, TEXT("ExecuteInTime"));

    if (ActorToApplyEffect && bIsChangeHealth && bIsApplied)
    {
        const auto HealthComponent = ActorToApplyEffect->FindComponentByClass<UTDSHealthComponent>();
        if (HealthComponent)
        {
            AmountValue > 0.f ? HealthComponent->TryToAddHealth(AmountValue) : HealthComponent->ReceiveDamage(FMath::Abs(AmountValue), nullptr);
        }
        if (HealthComponent && HealthComponent->IsHealthFull() && AmountValue > 0.f)
        {
            GetWorld()->GetTimerManager().ClearTimer(ExecutionTimer);
        }
    }

    const auto ActorWithInterface = Cast<ITDSActorInterface>(ActorToApplyEffect);
    if (ActorWithInterface && bCanStun && bIsApplied)
    {
        ActorWithInterface->MakeStun();
    }
}
//===============================================================
void UTSDPermanentStateEffect::InitObject(AActor* ActorToApply, FName BoneName)
{
    Super::InitObject(ActorToApply, BoneName);

    Execute();
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(ExecutionTimer, this, &UTSDPermanentStateEffect::Execute, ExecutionRate, true);
    }
}

void UTSDPermanentStateEffect::DestroyObject()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().ClearTimer(ExecutionTimer);
    }

    Super::DestroyObject();
}

void UTSDPermanentStateEffect::Execute()
{
    if (GetWorld() && ActorToApplyEffect && bIsApplied)
    {
        DrawDebugSphere(GetWorld(), ActorToApplyEffect->GetActorLocation(), AuraRadius, 24, FColor::Yellow, false, ExecutionRate, 0, 1.f);

        UGameplayStatics::ApplyRadialDamage(GetWorld(),                              //
                                            AmountValue,                             //
                                            ActorToApplyEffect->GetActorLocation(),  //
                                            AuraRadius,                              //
                                            UDamageType::StaticClass(),              //
                                            {ActorToApplyEffect},                    //
                                            ActorToApplyEffect,                      //
                                            nullptr,                                 //
                                            false);
    }
}
