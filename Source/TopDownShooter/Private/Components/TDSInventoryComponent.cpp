// This is educational project

#include "Components/TDSInventoryComponent.h"
#include "Weapon/TDSDefaultWeapon.h"
#include "TDSGameInstance.h"
#include "GameFrameWork/Character.h"
#include "Player/TDSCharacter.h"
#include "Player/TDSPlayerState.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(InventoryComponent_LOG, All, All);

UTDSInventoryComponent::UTDSInventoryComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UTDSInventoryComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UTDSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSInventoryComponent, CurrentWeapon);
    DOREPLIFETIME(UTDSInventoryComponent, WeaponSlots);
    DOREPLIFETIME(UTDSInventoryComponent, AmmoSlots);
    DOREPLIFETIME(UTDSInventoryComponent, ActiveWeaponSlotIndex);
}

void UTDSInventoryComponent::SetWeaponStartFire(bool bIsFire)
{
    if (!CurrentWeapon) return;
    CurrentWeapon->SetWeaponStartFire_OnServer(bIsFire);
}

void UTDSInventoryComponent::TryToSwitchToNextWeapon()
{
    TryToSwitchWeapon_OnServer(true);
}

void UTDSInventoryComponent::TryToSwitchToPrevWeapon()
{
    TryToSwitchWeapon_OnServer(false);
}

void UTDSInventoryComponent::TryToReloadWeapon()
{
    if (!CurrentWeapon) return;
    CurrentWeapon->ReloadWeapon_OnServer();
}

void UTDSInventoryComponent::UpdateStateWeapon(EMovementState MovementState)
{
    if (!CurrentWeapon) return;
    CurrentWeapon->UpdateStateWeapon_OnServer(MovementState);
}

void UTDSInventoryComponent::UpdateWeaponSlotsAdditionalData(uint8 WeaponSlotIndex, const FAdditionalWeaponData& Data)
{
    if (!WeaponSlots.IsValidIndex(WeaponSlotIndex)) return;

    WeaponSlots[WeaponSlotIndex].AdditionalData = Data;
    // OnAmmoInWeaponChanged.Broadcast(WeaponSlotIndex, Data.AmmoInWeapon);
    OnAmmoInWeaponChanged_OnClient(WeaponSlotIndex, Data.AmmoInWeapon);
    UE_LOG(InventoryComponent_LOG, Display, TEXT("Ammo in %s: %i goes to inventory"), *WeaponSlots[WeaponSlotIndex].WeaponName.ToString(), Data.AmmoInWeapon);
}

void UTDSInventoryComponent::LoadInventory()
{
    InitInventory_OnServer();
    // EquipWeaponFromSlot(ActiveWeaponSlotIndex);
    UE_LOG(InventoryComponent_LOG, Display, TEXT("Inventory Info Loaded"));
}

void UTDSInventoryComponent::SaveInventory()
{
    if (!GetOwner() || !GetOwner()->GetInstigatorController()) return;
    const auto PlayerState = Cast<ATDSPlayerState>(GetOwner()->GetInstigatorController()->PlayerState);
    if (!PlayerState) return;
    PlayerState->SaveInventory(WeaponSlots, AmmoSlots);
}

bool UTDSInventoryComponent::UpdateAmmunition(EWeaponType Type, int32 DeltaAmmo)
{
    for (auto& AmmoSlot : AmmoSlots)
    {
        if (AmmoSlot.WeaponType != Type) continue;

        if (DeltaAmmo > 0 && AmmoSlot.CarriedAmmo == AmmoSlot.MaxCarriedAmmo) return false;

        int32 NewCarriedAmmo = AmmoSlot.CarriedAmmo + DeltaAmmo;
        AmmoSlot.CarriedAmmo = FMath::Clamp(NewCarriedAmmo, 0, AmmoSlot.MaxCarriedAmmo);

        OnInventoryAmmoChanged_OnClient(Type, AmmoSlot.CarriedAmmo);
        // OnInventoryAmmoChanged.Broadcast(Type, AmmoSlot.CarriedAmmo);
        return true;
    }
    return false;
}

void UTDSInventoryComponent::OnReloadingStart(UAnimMontage* Animation)
{
    const auto RequiredAmmoSlot = AmmoSlots.FindByPredicate([&](const FAmmoSlot& AmmoSlot) { return AmmoSlot.WeaponType == CurrentWeapon->GetWeaponData().WeaponType; });
    if (!RequiredAmmoSlot) return;

    if (RequiredAmmoSlot->CarriedAmmo == 0)
    {
        CurrentWeapon->CancelReloading();
        return;
    }
    // OnReloadindStart.Broadcast(ActiveWeaponSlotIndex);
    OnReloadindStart_OnClient(ActiveWeaponSlotIndex);
    ReloadingAnimationStarts.Broadcast(Animation);
}

void UTDSInventoryComponent::OnReloadEnd(bool bIsSuccess, int32& AmmoInWeapon)
{
    if (!bIsSuccess) return;
    TakeAmmoFromInventory(AmmoInWeapon);
    UpdateWeaponSlotsAdditionalData(ActiveWeaponSlotIndex, CurrentWeapon->GetAdditionalWeaponData());
    OnReloadindEnd.Broadcast(ActiveWeaponSlotIndex);
}

void UTDSInventoryComponent::OnWeaponMakeShot(EWeaponShotType ShotType)
{
    /* from delegate handled on server */
    if (!CurrentWeapon) return;
    UpdateWeaponSlotsAdditionalData(ActiveWeaponSlotIndex, CurrentWeapon->GetAdditionalWeaponData());
    WeaponMakeShot.Broadcast(ShotType);
}

void UTDSInventoryComponent::AddWeaponToInventory_OnServer_Implementation(AActor* ActorToPickup, const FWeaponSlot& NewWeaponSlotData)
{
    if (!IsValidWeaponName(NewWeaponSlotData.WeaponName))
    {
        UE_LOG(InventoryComponent_LOG, Display, TEXT("Couldn't add weapon to inventory"));
    }

    int32 SlotIndex{0};
    bool bCanDestroy{false};
    if (FindWeponSlotIndexByWeaponID(NAME_None, SlotIndex))
    {
        WeaponSlots[SlotIndex] = NewWeaponSlotData;
        OnInventoryWeaponInfoChanged_OnClient(SlotIndex, NewWeaponSlotData);
        bCanDestroy = true;
    }
    else
    {
        UpdateWeaponSlotsAdditionalData(ActiveWeaponSlotIndex, CurrentWeapon->GetAdditionalWeaponData());
        DropWeapon(CurrentWeapon->GetWeaponMesh(), WeaponSlots[ActiveWeaponSlotIndex]);
        WeaponSlots[ActiveWeaponSlotIndex] = NewWeaponSlotData;
        OnInventoryWeaponInfoChanged_OnClient(ActiveWeaponSlotIndex, NewWeaponSlotData);
        EquipWeaponFromSlot(ActiveWeaponSlotIndex);
        bCanDestroy = true;
    }

    if (ActorToPickup && bCanDestroy)
    {
        ActorToPickup->Destroy();
    }
}

void UTDSInventoryComponent::InitInventory_OnServer_Implementation()
{
    if (!GetOwner() || !GetOwner()->GetInstigatorController()) return;
    const auto PlayerState = Cast<ATDSPlayerState>(GetOwner()->GetInstigatorController()->PlayerState);
    if (!PlayerState) return;

    PlayerState->LoadInventory(WeaponSlots, AmmoSlots);

    if (WeaponSlots.Num() == 0)
    {
        UE_LOG(InventoryComponent_LOG, Warning, TEXT("Weapon slots not found"));
        return;
    }

    for (auto& WeaponSlot : WeaponSlots)
    {
        if (WeaponSlot.WeaponName.IsNone()) continue;

        if (IsValidWeaponName(WeaponSlot.WeaponName)) continue;

        WeaponSlot = {nullptr, NAME_None, 0, 0};
        UE_LOG(InventoryComponent_LOG, Display, TEXT("WeaponName set as None, AdditionalWeponData set as Default"));
    }

    UE_LOG(InventoryComponent_LOG, Display, TEXT("InitInventory done"));

    EquipWeaponFromSlot(ActiveWeaponSlotIndex);
}

bool UTDSInventoryComponent::EquipWeaponFromSlot(int8& NewIndex)
{
    if (WeaponSlots.Num() == 0)
    {
        UE_LOG(InventoryComponent_LOG, Warning, TEXT("Weapon slots not found"));
        return false;
    }

    if (NewIndex > WeaponSlots.Num())
    {
        NewIndex = 0;
    }
    if (NewIndex < 0)
    {
        NewIndex = WeaponSlots.Num() - 1;
    }
    UE_LOG(InventoryComponent_LOG, Display, TEXT("Equip Weapon From Slot %i"), NewIndex);

    const auto NewWeaponName = WeaponSlots[NewIndex].WeaponName;

    if (NewWeaponName.IsNone())
    {
        UE_LOG(InventoryComponent_LOG, Display, TEXT("Empty Weapon slot"));
        return false;
    }

    /* this condition is due to the replacement of weapons in the slot or switching to another slot */
    if (CurrentWeapon && ActiveWeaponSlotIndex != NewIndex)
    {
        UpdateWeaponSlotsAdditionalData(ActiveWeaponSlotIndex, CurrentWeapon->GetAdditionalWeaponData());
        OnReloadindEnd.Broadcast(ActiveWeaponSlotIndex);
    }

    const auto NewAdditionalData = WeaponSlots[NewIndex].AdditionalData;
    SpawnWeapon(NewWeaponName, NewAdditionalData);

    if (!CurrentWeapon) return false;

    if (ActiveWeaponSlotIndex != NewIndex)
    {
        ActiveWeaponSlotIndex = NewIndex;
    }

    // OnWeaponEquipped.Broadcast(ActiveWeaponSlotIndex);
    OnWeaponEquipped_OnClient(ActiveWeaponSlotIndex);

    return true;
}

void UTDSInventoryComponent::SpawnWeapon(FName WeaponID, FAdditionalWeaponData Data)
{
    auto Character = Cast<ACharacter>(GetOwner());
    if (!Character) return;

    if (CurrentWeapon)
    {
        CurrentWeapon->Destroy();
        CurrentWeapon = nullptr;
    }

    const auto TDSGameInstance = Cast<UTDSGameInstance>(Character->GetGameInstance());
    if (!TDSGameInstance) return;

    FWeaponData WeaponData;
    if (!TDSGameInstance->GetWeaponDataByName(WeaponID, WeaponData))
    {
        UE_LOG(InventoryComponent_LOG, Warning, TEXT("Spawn Canceled! Weapon not found in Weapon Data Table"));
        return;
    }

    FActorSpawnParameters SpawnParams;
    SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    SpawnParams.Owner = Character;
    SpawnParams.Instigator = Character->GetInstigator();

    CurrentWeapon = GetWorld()->SpawnActor<ATDSDefaultWeapon>(WeaponData.WeaponClass, SpawnParams);

    if (!CurrentWeapon || !Character->GetMesh()) return;

    CurrentWeapon->SetWeaponID(WeaponID);
    CurrentWeapon->SetAdditionalWeaponData(Data);
    CurrentWeapon->SetWeaponData(WeaponData);

    FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
    CurrentWeapon->AttachToComponent(Character->GetMesh(), AttachmentRules, "WeaponSocket");

    CurrentWeapon->OnReloadStart.AddUObject(this, &UTDSInventoryComponent::OnReloadingStart);
    CurrentWeapon->OnReloadEnd.AddUObject(this, &UTDSInventoryComponent::OnReloadEnd);
    CurrentWeapon->OnWeaponMakeShot.AddUObject(this, &UTDSInventoryComponent::OnWeaponMakeShot);

    auto Player = Cast<ATDSCharacter>(GetOwner());
    if (!Player) return;
    CurrentWeapon->UpdateStateWeapon_OnServer(Player->GetCurrentMovementState());
}

bool UTDSInventoryComponent::IsValidWeaponName(FName WeaponName)
{
    if (!GetWorld()) return false;

    const auto TDSGameInstance = GetWorld()->GetGameInstance<UTDSGameInstance>();
    if (!TDSGameInstance) return false;

    if (TDSGameInstance->IsValidWeaponName(WeaponName)) return true;

    UE_LOG(InventoryComponent_LOG, Warning, TEXT("%s not found in Weapon Data Table"), *WeaponName.ToString());
    return false;
}

void UTDSInventoryComponent::TryToSwitchWeapon_OnServer_Implementation(bool bIsNextWeapon)
{
    if (WeaponSlots.Num() < 2) return;
    UE_LOG(InventoryComponent_LOG, Display, TEXT("ActiveWeaponSlotIndex: %i"), ActiveWeaponSlotIndex);

    int8 IndexModifier = bIsNextWeapon ? 1 : -1;
    int8 NewWeaponSlot = (ActiveWeaponSlotIndex + IndexModifier) % WeaponSlots.Num();
    UE_LOG(InventoryComponent_LOG, Display, TEXT("NewWeaponSlot: %i"), NewWeaponSlot);

    int8 IterationNum{0};
    while (!EquipWeaponFromSlot(NewWeaponSlot))
    {
        NewWeaponSlot = (NewWeaponSlot + IndexModifier) % WeaponSlots.Num();
        ++IterationNum;

        if (IterationNum >= WeaponSlots.Num()) break;
    }
}

void UTDSInventoryComponent::TakeAmmoFromInventory(int32& AmmoInWeapon)
{
    if (!CurrentWeapon) return;

    int32 AmmoNeeded = CurrentWeapon->GetWeaponData().MaxAmmo - AmmoInWeapon;

    const auto RequiredAmmoSlot = AmmoSlots.FindByPredicate([&](const FAmmoSlot& AmmoSlot) { return AmmoSlot.WeaponType == CurrentWeapon->GetWeaponData().WeaponType; });

    if (!RequiredAmmoSlot) return;

    AmmoInWeapon = AmmoNeeded > RequiredAmmoSlot->CarriedAmmo ? RequiredAmmoSlot->CarriedAmmo : CurrentWeapon->GetWeaponData().MaxAmmo;

    UpdateAmmunition(CurrentWeapon->GetWeaponData().WeaponType, -AmmoNeeded);
    UE_LOG(InventoryComponent_LOG, Display, TEXT("Carried ammo: %i"), RequiredAmmoSlot->CarriedAmmo);
}

bool UTDSInventoryComponent::FindWeponSlotIndexByWeaponID(const FName WeaponID, int32& WeaponSlotIndex)
{
    int32 SlotIndex{0};
    for (const auto& WeaponSlot : WeaponSlots)
    {
        if (WeaponSlot.WeaponName == WeaponID)
        {
            WeaponSlotIndex = SlotIndex;
            return true;
        }
        ++SlotIndex;
    }
    return false;
}

void UTDSInventoryComponent::DropWeapon(UMeshComponent* Mesh, const FWeaponSlot& WeaponSlotData)
{
    /* handled on server */
    // if (!GetOwner() || !CurrentWeapon || !CurrentWeapon->GetWeaponMesh()) return;
    if (!GetOwner() || !Mesh) return;

    const auto Player = Cast<ATDSCharacter>(GetOwner());
    if (!Player) return;

    // Player->DropWeapon_BP(CurrentWeapon->GetWeaponMesh(), WeaponSlots[ActiveWeaponSlotIndex]);
    Player->DropWeapon_BP(Mesh, WeaponSlotData);
}

void UTDSInventoryComponent::OnAmmoInWeaponChanged_OnClient_Implementation(uint8 WeaponSlotIndex, int32 AmmoInWeapon)
{
    OnAmmoInWeaponChanged.Broadcast(WeaponSlotIndex, AmmoInWeapon);
}

void UTDSInventoryComponent::OnWeaponEquipped_OnClient_Implementation(int8 SlotIndex)
{
    OnWeaponEquipped.Broadcast(SlotIndex);
}

void UTDSInventoryComponent::OnInventoryWeaponInfoChanged_OnClient_Implementation(uint8 SlotIndex, const FWeaponSlot& WeaponSlot)
{
    OnInventoryWeaponInfoChanged.Broadcast(SlotIndex, WeaponSlot);
}

void UTDSInventoryComponent::OnInventoryAmmoChanged_OnClient_Implementation(EWeaponType Type, int32 Ammo)
{
    OnInventoryAmmoChanged.Broadcast(Type, Ammo);
}

void UTDSInventoryComponent::OnReloadindStart_OnClient_Implementation(uint8 WeaponSlotIndex)
{
    OnReloadindStart.Broadcast(WeaponSlotIndex);
}