// This is educational project

#include "Components/TDSHealthComponent.h"
#include "Weapon/TDSDefaultProjectile.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Buff/TDSStateEffect.h"
#include "TDSGameModeBase.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"

DEFINE_LOG_CATEGORY_STATIC(HealthComponent_LOG, All, All);

UTDSHealthComponent::UTDSHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
    SetIsReplicatedByDefault(true);
}

void UTDSHealthComponent::SetHealth(float NewHealth)
{
    /* handled on server */
    float DeltaHealth = NewHealth - Health;
    Health = FMath::Clamp(NewHealth, 0.f, MaxHealth);
    OnHealthChanged_OnClient(Health, MaxHealth, DeltaHealth);
    UE_LOG(HealthComponent_LOG, Display, TEXT("SetHealth: %f"), Health);
}

void UTDSHealthComponent::ReceiveDamage(float Damage, AController* InstigatedBy)
{
    /* handled on server */
    // UE_LOG(HealthComponent_LOG, Warning, TEXT(" % s reciev % f damage"), *GetOwner()->GetName(), Damage);
    if (Damage <= 0.f || bIsInvulnerable || IsDead()) return;

    SetHealth(Health - Damage * DamageReducingFactor);

    if (IsDead())
    {
        Killed(InstigatedBy);
        AppliedStateEffects.Empty();  /////!!!!
        OnDeath.Broadcast();
        return;
    }
}

bool UTDSHealthComponent::TryToAddHealth(float HealthAmount)
{
    /* handled on server, if used in blueprint should call only from server */
    if (IsDead() || IsHealthFull()) return false;
    SetHealth(Health + HealthAmount);
    return true;
}

void UTDSHealthComponent::RemoveStateEffect(UTDSStateEffect* EffectToRemove)
{
    /* handled on server */
    if (!EffectToRemove) return;
    AppliedStateEffects.Remove(EffectToRemove);

    SpawnOrRemoveStateEffectFX(EffectToRemove, false);
    /* it will call RemoveStateEffect_OnRep() on client */
    StateEffectToRemove = EffectToRemove;
}

void UTDSHealthComponent::AddNewStateEffect(UTDSStateEffect* EffectToAdd)
{
    /* handled on server */
    AppliedStateEffects.Add(EffectToAdd);
    // OnStateEffectApplied.Broadcast(EffectToAdd->GetStateEffectData());

    SpawnOrRemoveStateEffectFX(EffectToAdd, true);
    /* it will call AddNewStateEffect_OnRep() on client */
    StateEffectToAdd = EffectToAdd;
}

bool UTDSHealthComponent::TryToApplyStateEffect(UTDSStateEffect* EffectToApply)
{
    /**
     * handled on server
     * by functions handled on server ApplyPointDamage() and ApplyRadialDamage() to
     * Types::ApplyStateEffectByPhysicalMaterial and then UTDSStateEffect::InitObject
     */
    if (!EffectToApply || IsDead()) return false;

    if (GetAppliedStateEffects().Num() == 0)
    {
        AddNewStateEffect(EffectToApply);
        return true;
    }

    bool bCanApply{true};
    for (const auto Effect : AppliedStateEffects)
    {
        if (Effect->GetStateEffectData().StateEffectName == EffectToApply->GetStateEffectData().StateEffectName)
        {
            bCanApply = EffectToApply->GetStateEffectData().bCanStack;
            break;
        }
    }

    if (bCanApply)
    {
        AddNewStateEffect(EffectToApply);
    }

    return bCanApply;
}

void UTDSHealthComponent::ApplyStateEffectFromPickup(TSubclassOf<UTDSStateEffect> StateEffect)
{
    /* if used in blueprint should call only from server  */
    if (IsDead()) return;
    /* checking if permanent effect alweady exist */
    for (const auto ExistingStateEffect : AppliedStateEffects)
    {
        if (ExistingStateEffect->GetClass() == StateEffect->GetClass())
        {
            AppliedStateEffects.Remove(ExistingStateEffect);
        }
    }

    UTypes::ApplyStateEffectByPhysicalMaterial(GetOwner(),   //
                                               StateEffect,  //
                                               GetOwnersPhysicalMaterial(GetOwner()));
}

void UTDSHealthComponent::BoostHealth(float BoostFactor)
{
    /* handled on server */
    checkf(BoostFactor > 0.f, TEXT("MaxHealth couldn't be less or equal zero"));
    MaxHealth *= BoostFactor;
    if (MaxHealth < Health)
    {
        SetHealth(MaxHealth);
    }
}

void UTDSHealthComponent::BeginPlay()
{
    Super::BeginPlay();

    SetHealth(MaxHealth);

    const auto ComponentOwner = GetOwner();
    if (ComponentOwner)
    {
        ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UTDSHealthComponent::OnTakeAnyDamage);
        ComponentOwner->OnTakePointDamage.AddDynamic(this, &UTDSHealthComponent::OnTakePointDamage);
        ComponentOwner->OnTakeRadialDamage.AddDynamic(this, &UTDSHealthComponent::OnTakeRadialDamage);
    }
}

void UTDSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSHealthComponent, Health);
    DOREPLIFETIME(UTDSHealthComponent, MaxHealth);
    DOREPLIFETIME(UTDSHealthComponent, AppliedStateEffects);
    DOREPLIFETIME(UTDSHealthComponent, StateEffectToAdd);
    DOREPLIFETIME(UTDSHealthComponent, StateEffectToRemove);
}

bool UTDSHealthComponent::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
    bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (auto Effect : AppliedStateEffects)
    {
        if (!Effect) continue;

        WroteSomething |= Channel->ReplicateSubobject(Effect, *Bunch, *RepFlags);
    }

    return WroteSomething;
}

void UTDSHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
    UE_LOG(HealthComponent_LOG, Display, TEXT("TakeAnyDamage"));
}

void UTDSHealthComponent::OnTakePointDamage(AActor* DamagedActor,
                                            float Damage,
                                            AController* InstigatedBy,
                                            FVector HitLocation,
                                            UPrimitiveComponent* FHitComponent,
                                            FName BoneName,
                                            FVector ShotFromDirection,
                                            const UDamageType* DamageType,
                                            AActor* DamageCauser)
{
    ReceiveDamage(Damage, InstigatedBy);
}

void UTDSHealthComponent::OnTakeRadialDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector Origin, FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
    ReceiveDamage(Damage, InstigatedBy);

    const auto PhysMaterial = GetOwnersPhysicalMaterial(DamagedActor);
    if (!PhysMaterial) return;

    const auto Projectile = Cast<ATDSDefaultProjectile>(DamageCauser);
    if (Projectile)
    {
        const auto StateEffectClass = Projectile->GetProjectileData().StateEffectClass;
        UTypes::ApplyStateEffectByPhysicalMaterial(DamagedActor, StateEffectClass, PhysMaterial);
    }
}

/**
 * To get current physical meterial needed set this
 * in material of actor's static mesh or character's skeletal mesh in editor.
 */
UPhysicalMaterial* UTDSHealthComponent::GetOwnersPhysicalMaterial(AActor* CheckingActor)
{
    if (const auto Character = Cast<ACharacter>(CheckingActor))
    {
        if (!Character->GetMesh()  //
            || !Character->GetMesh()->GetMaterial(0))
            return nullptr;

        return Character->GetMesh()->GetMaterial(0)->GetPhysicalMaterial();
    }
    else if (const auto FoundMesh = CheckingActor->FindComponentByClass<UStaticMeshComponent>())
    {
        return FoundMesh->GetMaterial(0)  //
                   ?
                   FoundMesh->GetMaterial(0)->GetPhysicalMaterial() :  //
                   nullptr;
    }
    return nullptr;
}

void UTDSHealthComponent::Killed(AController* KillerController)
{
    /* handled on server */
    if (!GetWorld()) return;
    const auto GameMode = Cast<ATDSGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode) return;

    const auto ComponentPawnOwner = Cast<APawn>(GetOwner());
    const auto VictimController = ComponentPawnOwner ? ComponentPawnOwner->Controller : nullptr;
    GameMode->Killed(VictimController, KillerController);
}

void UTDSHealthComponent::OnHealthChanged_OnClient_Implementation(float InHealth, float InMaxHealth, float DeltaHealth)
{
    OnHealthChanged.Broadcast(InHealth, InMaxHealth, DeltaHealth);
}

void UTDSHealthComponent::AddNewStateEffect_OnRep()
{
    /* handled only on client */
    if (!StateEffectToAdd) return;

    SpawnOrRemoveStateEffectFX(StateEffectToAdd, true);
}

void UTDSHealthComponent::RemoveStateEffect_OnRep()
{
    /* handled only on client */
    if (!StateEffectToRemove) return;

    SpawnOrRemoveStateEffectFX(StateEffectToRemove, false);
}

void UTDSHealthComponent::SpawnOrRemoveStateEffectFX(UTDSStateEffect* StateEffect, bool bIsAdd)
{
    if (!StateEffect || !GetOwner()) return;
    UE_LOG(HealthComponent_LOG, Display, TEXT("SpawnOrRemoveStateEffectFX: %s"), *StateEffect->GetName());

    const auto VisualEffect = StateEffect->GetStateEffectData().VisualEffect;
    const auto BoneName = StateEffect->GetBoneToApplyEffectName();
    const auto bIsPermanent = StateEffect->GetStateEffectData().bIsPermanentEffect;

    if (!bIsPermanent)
    {
        SpawnStateEffectFX_Multicast(VisualEffect, BoneName);
        return;
    }

    bIsAdd ? SpawnAndWritePermanentStateEffectFX(VisualEffect, BoneName) : RemovePermanentStateEffectFX(VisualEffect);
}

void UTDSHealthComponent::SpawnAndWritePermanentStateEffectFX(UNiagaraSystem* VisualEffect, FName BoneName)
{
    const auto ComponentToAdd = SpawnStateEffectFX(VisualEffect, BoneName);
    if (ComponentToAdd)
    {
        StateEffectNiagaraComponents.Add(ComponentToAdd);
    }
}

void UTDSHealthComponent::RemovePermanentStateEffectFX(UNiagaraSystem* VisualEffect)
{
    for (auto Component : StateEffectNiagaraComponents)
    {
        if (Component->GetAsset() != VisualEffect) continue;
        Component->DestroyComponent();
        StateEffectNiagaraComponents.Remove(Component);
        break;
    }
}

UNiagaraComponent* UTDSHealthComponent::SpawnStateEffectFX(UNiagaraSystem* VisualEffect, FName BoneName)
{
    if (!VisualEffect || !GetOwner()) return nullptr;

    if (!BoneName.IsNone())
    {
        if (const auto Mesh = GetOwner()->FindComponentByClass<USkeletalMeshComponent>())
        {
            return UNiagaraFunctionLibrary::SpawnSystemAttached(VisualEffect,                   //
                                                                Mesh,                           //
                                                                BoneName,                       //
                                                                FVector::ZeroVector,            //
                                                                FRotator::ZeroRotator,          //
                                                                EAttachLocation::SnapToTarget,  //
                                                                false);
        }
    }
    else
    {
        return UNiagaraFunctionLibrary::SpawnSystemAttached(VisualEffect,                    //
                                                            GetOwner()->GetRootComponent(),  //
                                                            NAME_None,                       //
                                                            FVector::ZeroVector,             //
                                                            FRotator::ZeroRotator,           //
                                                            EAttachLocation::SnapToTarget,   //
                                                            false);
    }

    return nullptr;
}

void UTDSHealthComponent::SpawnStateEffectFX_Multicast_Implementation(UNiagaraSystem* VisualEffect, FName BoneName)
{
    SpawnStateEffectFX(VisualEffect, BoneName);
}
