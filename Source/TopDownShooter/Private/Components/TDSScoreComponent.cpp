// This is educational project

#include "Components/TDSScoreComponent.h"

UTDSScoreComponent::UTDSScoreComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UTDSScoreComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UTDSScoreComponent::SetScorePoints(int32 Points)
{
    ScorePoints = Points;
}
