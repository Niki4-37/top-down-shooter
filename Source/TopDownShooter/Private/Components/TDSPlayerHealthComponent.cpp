// This is educational project

#include "Components/TDSPlayerHealthComponent.h"

#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Net/UnrealNetwork.h"

UTDSPlayerHealthComponent::UTDSPlayerHealthComponent() {}

void UTDSPlayerHealthComponent::UpdateStaminaState(EMovementState State)
{
    if (State == EMovementState::Sprint_State)
    {
        GetWorld()->GetTimerManager().ClearTimer(RegenerationStaminaTimer);

        StartStaminaRegeneration(false);
    }
    else
    {
        StartStaminaRegeneration(true);
    }
}

void UTDSPlayerHealthComponent::ChangeStamina(float Points)
{
    if (IsDead())
    {
        GetWorld()->GetTimerManager().ClearTimer(RegenerationStaminaTimer);
        return;
    }
    const float NewStamina{CurrentStamina + Points};
    CurrentStamina = FMath::Clamp(NewStamina, 0.f, MaxStamina);
    OnStaminaChanged.Broadcast(CurrentStamina);
    OnStaminaIsOver.Broadcast(FMath::IsNearlyZero(CurrentStamina));
}

void UTDSPlayerHealthComponent::BeginPlay()
{
    Super::BeginPlay();

    check(GetWorld());

    Shield = MaxShield;
    OnShieldChanged.Broadcast(Shield);
    ChangeStamina(MaxStamina);
}

void UTDSPlayerHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSPlayerHealthComponent, Shield);
}

void UTDSPlayerHealthComponent::ReceiveDamage(float Damage, AController* InstigatedBy)
{
    /* handled on server */
    if (IsDead()) return;
    UE_LOG(LogTemp, Display, TEXT("PlayerHealthComponent reciev damage: %f"), Damage);
    float FinalDamage{0.f};
    if (Damage < Shield)
    {
        Shield -= Damage;

        SpawnShieldImpactFX_Multicast(ShieldImpactEffect);
        SpawnShieldImpactSound_Multicast(ShieldImpactSound);
    }
    else
    {
        FinalDamage = Damage - Shield;
        Shield = 0;

        // SpawnShieldImpactFX_Multicast(ShieldDestroyEffect);
        // SpawnShieldImpactSound_Multicast(ShieldDestroySound);
    }

    OnShieldChanged.Broadcast(Shield);

    GetWorld()->GetTimerManager().ClearTimer(ShieldCooldownTimer);

    GetWorld()->GetTimerManager().SetTimer(ShieldCooldownTimer,                        //
                                           this,                                       //
                                           &UTDSPlayerHealthComponent::RestoreShield,  //
                                           RestoreRate,                                //
                                           true,                                       //
                                           CooldownDelay);

    Super::ReceiveDamage(FinalDamage, InstigatedBy);
}

void UTDSPlayerHealthComponent::RestoreShield()
{
    if (FMath::IsNearlyEqual(Shield, MaxShield) || IsDead())
    {
        GetWorld()->GetTimerManager().ClearTimer(ShieldCooldownTimer);
        return;
    }
    Shield += ShieldModifier;
    OnShieldChanged.Broadcast(Shield);
    UE_LOG(LogTemp, Display, TEXT("PlayerHealthComponent shield: %f"), Shield);
}

void UTDSPlayerHealthComponent::StartStaminaRegeneration(const bool Enable)
{
    FTimerDelegate RegenDelegate;
    const float Points = Enable ? RegenerationStaminaPoints : -ReduceStaminaPoints;
    RegenDelegate.BindUFunction(this, "ChangeStamina", Points);
    const float Delay = Enable ? StaminaRegenerationDelay : 0.f;
    GetWorld()->GetTimerManager().SetTimer(RegenerationStaminaTimer, RegenDelegate, 0.1f, true, Delay);
}

void UTDSPlayerHealthComponent::ShieldChanged_OnRep()
{
    /* handled only on client */
    OnShieldChanged.Broadcast(Shield);
}

void UTDSPlayerHealthComponent::SpawnShieldImpactFX_Multicast_Implementation(UNiagaraSystem* Effect)
{
    UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), Effect, GetOwner()->GetActorLocation(), FRotator::ZeroRotator);
}

void UTDSPlayerHealthComponent::SpawnShieldImpactSound_Multicast_Implementation(USoundCue* Sound)
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetOwner()->GetActorLocation());
}
