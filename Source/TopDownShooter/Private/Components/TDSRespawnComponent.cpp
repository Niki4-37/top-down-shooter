// This is educational project

#include "Components/TDSRespawnComponent.h"
#include "TDSGameModeBase.h"

UTDSRespawnComponent::UTDSRespawnComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UTDSRespawnComponent::StartRespawning(float TimeToRespawn)
{
    if (!GetWorld()) return;

    GetWorld()->GetTimerManager().SetTimer(RespawningTimer, this, &UTDSRespawnComponent::RespawnOwner, TimeToRespawn, false);
}

void UTDSRespawnComponent::RespawnOwner()
{
    if (!GetWorld()) return;

    GetWorld()->GetTimerManager().ClearTimer(RespawningTimer);

    const auto GameMode = Cast<ATDSGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode) return;

    GameMode->TryToRespawn(Cast<AController>(GetOwner()));
}
