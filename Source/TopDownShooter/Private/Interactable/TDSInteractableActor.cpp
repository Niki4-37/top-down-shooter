// This is educational project

#include "Interactable/TDSInteractableActor.h"
#include "Materials/MaterialInterface.h"

ATDSInteractableActor::ATDSInteractableActor()
{
    PrimaryActorTick.bCanEverTick = false;
    bReplicates = true;
}

void ATDSInteractableActor::BeginPlay()
{
    Super::BeginPlay();
}

bool ATDSInteractableActor::AvalableForEffects_CPP()
{
    return false;
}

UPhysicalMaterial* ATDSInteractableActor::GetPhysicalMaterial()
{
    const auto FoundMesh = FindComponentByClass<UStaticMeshComponent>();
    if (!FoundMesh) return nullptr;

    const auto MeshMaterial = FoundMesh->GetMaterial(0);
    if (!MeshMaterial) return nullptr;

    return MeshMaterial->GetPhysicalMaterial();
}