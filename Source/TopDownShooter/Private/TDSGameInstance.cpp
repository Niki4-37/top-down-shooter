// This is educational project

#include "TDSGameInstance.h"

DEFINE_LOG_CATEGORY_STATIC(TDSGameInstance_LOG, All, All);

bool UTDSGameInstance::GetWeaponDataByName(FName WeaponName, FWeaponData& OutData) const
{
    if (!WeaponInfoTable)
    {
        UE_LOG(TDSGameInstance_LOG, Warning, TEXT("Data table is forgotten to set"));
        return false;
    }

    const auto WeaponInfoRaw = WeaponInfoTable->FindRow<FWeaponData>(WeaponName, "", false);
    if (!WeaponInfoRaw) return false;

    OutData = *WeaponInfoRaw;
    return true;
}

bool UTDSGameInstance::IsValidWeaponName(FName WeaponNameToCheck) const
{
    if (!WeaponInfoTable)
    {
        UE_LOG(TDSGameInstance_LOG, Warning, TEXT("Data table is forgotten to set"));
        return false;
    }

    TArray<FName> WeaponNamesFromTable = WeaponInfoTable->GetRowNames();
    for (const auto& WeaponName : WeaponNamesFromTable)
    {
        if (WeaponNameToCheck == WeaponName) return true;
    }

    return false;
}

bool UTDSGameInstance::GetLevelDataByName(FName LevelName, FLevelData& OutData) const
{
    if (!LevelInfoTable)
    {
        UE_LOG(TDSGameInstance_LOG, Warning, TEXT("Data table is forgotten to set"));
        return false;
    }

    const auto LevelInfoRaw = LevelInfoTable->FindRow<FLevelData>(LevelName, "", false);
    if (!LevelInfoRaw) return false;

    OutData = *LevelInfoRaw;
    return true;
}
