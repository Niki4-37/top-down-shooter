// This is educational project

#include "TDSGameStateBase.h"
#include "Net/UnrealNetwork.h"
#include "TDSGameModeBase.h"
#include "GameFramework/PlayerState.h"
#include "Components/TDSHealthComponent.h"

APawn* ATDSGameStateBase::GetFirstAlivePawn() const
{
    for (const auto PlayerState : PlayerArray)
    {
        if (PlayerState->IsSpectator()) continue;
        return PlayerState->GetPawn();
    }
    return nullptr;
}

void ATDSGameStateBase::HandleBeginPlay()
{
    Super::HandleBeginPlay();

    const auto TDSGameMode = Cast<ATDSGameModeBase>(AuthorityGameMode);
    if (TDSGameMode)
    {
        TDSGameMode->OnProgressPointsChanged.AddDynamic(this, &ATDSGameStateBase::OnProgressPointsChanged);
        TDSGameMode->OnPhasePointsChanged.AddDynamic(this, &ATDSGameStateBase::OnPhasePointsChanged);
    }
}

void ATDSGameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSGameStateBase, ProgressPoints);
    DOREPLIFETIME(ATDSGameStateBase, PhasePoints);
}

void ATDSGameStateBase::OnProgressPointsChanged(int32 Points)
{
    ProgressPoints = Points;
    const float Persent = PhasePoints ? (static_cast<float>(ProgressPoints) / PhasePoints) : 0;
    PercentProgerssChanged.Broadcast(Persent);
}

void ATDSGameStateBase::OnPhasePointsChanged(int32 Points)
{
    PhasePoints = Points;
}

void ATDSGameStateBase::ProgresPointsChanged_OnRep()
{
    PercentProgerssChanged.Broadcast(GetPhaseProgress());
}
