// This is educational project

#include "Interfaces/TDSActorInterface.h"

// Add default functionality here for any ITDSActorInterface functions that are not pure virtual.

bool ITDSActorInterface::AvalableForEffects_CPP()
{
    return true;
}

UPhysicalMaterial* ITDSActorInterface::GetPhysicalMaterial()
{
    return nullptr;
}
