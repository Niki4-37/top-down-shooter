// This is educational project

#include "AI/TDSEnemy.h"
#include "Components/CapsuleComponent.h"
#include "Components/TDSHealthComponent.h"
#include "AI/TDSAIController.h"
#include "GameFramework/CharacterMovementComponent.h"

ATDSEnemy::ATDSEnemy()
{
    AutoPossessAI = EAutoPossessAI::Disabled;
    AIControllerClass = ATDSAIController::StaticClass();

    bUseControllerRotationYaw = false;
    if (GetCharacterMovement())
    {
        GetCharacterMovement()->bUseControllerDesiredRotation = true;
        GetCharacterMovement()->RotationRate = FRotator(0.0f, 200.0f, 0.0f);
    }

    PrimaryActorTick.bCanEverTick = false;

    HealthComponent = CreateDefaultSubobject<UTDSHealthComponent>("HealthComponent");
}

void ATDSEnemy::BeginPlay()
{
    Super::BeginPlay();

    check(HealthComponent);
}

void ATDSEnemy::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    if (HealthComponent)
    {
        HealthComponent->OnDeath.AddUniqueDynamic(this, &ATDSEnemy::OnDeath);
        HealthComponent->OnHealthChanged.AddUniqueDynamic(this, &ATDSEnemy::OnHealthChanged);
    }

    if (GetMesh() && GetMesh()->GetAnimInstance())
    {
        GetMesh()->GetAnimInstance()->OnMontageEnded.AddDynamic(this, &ATDSEnemy::OnMontageEnded);
        GetMesh()->GetAnimInstance()->OnMontageStarted.AddDynamic(this, &ATDSEnemy::FOnMontageStarted);
    }
}

void ATDSEnemy::OnDeath()
{
    /* from delegate which runs on server */
    GetMovementComponent()->StopMovementImmediately();
    SwithcToRagdollAfterDeath_Multicast();
}

void ATDSEnemy::OnHealthChanged(float Health, float MaxHealth, float DeltaHealth)
{
    /* set the probability of an event for playing an AnimMontage */
}

void ATDSEnemy::GetScorePoints(int32& OutPoints) const
{
    OutPoints = ScorePoints;
}

void ATDSEnemy::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
    if (Montage == ReactionAnimMontage)
    {
    }
    // UE_LOG(LogTemp, Display, TEXT("Montage End Notify"));
}

void ATDSEnemy::FOnMontageStarted(UAnimMontage* Montage)
{
    if (Montage == ReactionAnimMontage)
    {
    }
    // UE_LOG(LogTemp, Display, TEXT("Montage Start Notify"));
}

void ATDSEnemy::SwithcToRagdollAfterDeath_Multicast_Implementation()
{
    if (!GetMesh() || !GetCapsuleComponent()) return;
    GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    GetMesh()->SetSimulatePhysics(true);
    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}
