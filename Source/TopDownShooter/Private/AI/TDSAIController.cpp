// This is educational project

#include "AI/TDSAIController.h"
#include "Components/TDSRespawnComponent.h"
#include "Components/TDSScoreComponent.h"
#include "AI/TDSEnemy.h"
#include "Interfaces/TDSActorInterface.h"

ATDSAIController::ATDSAIController()
{
    RespawnComponent = CreateDefaultSubobject<UTDSRespawnComponent>("RespawnComponent");
    ScoreComponent = CreateDefaultSubobject<UTDSScoreComponent>("ScoreComponent");

    // bWantsPlayerState = true;
}

void ATDSAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    if (const auto AIEnemy = Cast<ATDSEnemy>(InPawn))
    {
        RunBehaviorTree(AIEnemy->BehaviorTreeAsset);
    }

    UpdateScoreInScoreComponent();
}

void ATDSAIController::UpdateScoreInScoreComponent()
{
    if (!ScoreComponent) return;
    const auto ActorWithInterface = Cast<ITDSActorInterface>(GetPawn());
    int32 PointsToSet{0};
    ActorWithInterface ? ActorWithInterface->GetScorePoints(PointsToSet) : PointsToSet;
    ScoreComponent->SetScorePoints(PointsToSet);
}
