// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TDSAIController.generated.h"

class UTDSRespawnComponent;
class UTDSScoreComponent;

UCLASS()
class TOPDOWNSHOOTER_API ATDSAIController : public AAIController
{
    GENERATED_BODY()

public:
    ATDSAIController();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UTDSRespawnComponent* RespawnComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UTDSScoreComponent* ScoreComponent;

    virtual void OnPossess(APawn* InPawn) override;

private:
    void UpdateScoreInScoreComponent();
};
