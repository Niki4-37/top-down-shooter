// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/TDSActorInterface.h"
#include "TDSEnemy.generated.h"

class UTDSHealthComponent;
class UBehaviorTree;
class UAnimMontage;

UCLASS()
class TOPDOWNSHOOTER_API ATDSEnemy : public ACharacter, public ITDSActorInterface
{
    GENERATED_BODY()

public:
    ATDSEnemy();

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
    UBehaviorTree* BehaviorTreeAsset;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UTDSHealthComponent* HealthComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
    UAnimMontage* ReactionAnimMontage;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
    TArray<UAnimMontage*> DeathAnimMontages;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Rules")
    int32 ScorePoints{1};

    virtual void BeginPlay() override;
    virtual void PostInitializeComponents() override;

private:
    UFUNCTION()
    void OnDeath();
    UFUNCTION()
    void OnHealthChanged(float Health, float MaxHealth, float DeltaHealth);

private:
    /* interface function ITDSActorInterface */
    virtual void GetScorePoints(int32& OutPoints) const override;

private:
    UFUNCTION()
    void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);
    UFUNCTION()
    void FOnMontageStarted(UAnimMontage* Montage);

    UFUNCTION(NetMulticast, Reliable)
    void SwithcToRagdollAfterDeath_Multicast();
};
