// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TDSGameInstance.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API UTDSGameInstance : public UGameInstance
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Settings")
    UDataTable* LevelInfoTable;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    UDataTable* WeaponInfoTable;

    UFUNCTION(BlueprintCallable)
    bool GetWeaponDataByName(FName WeaponName, FWeaponData& OutData) const;

    bool IsValidWeaponName(FName WeaponNameToCheck) const;

    bool GetLevelDataByName(FName LevelName, FLevelData& OutData) const;
};
