// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Interfaces/TDSActorInterface.h"
#include "TDSCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UDecalComponent;
class UMaterialInterface;
class UTextRenderComponent;
class ATDSDefaultWeapon;
class UTDSInventoryComponent;
class UTDSPlayerHealthComponent;

UCLASS()
class TOPDOWNSHOOTER_API ATDSCharacter : public ACharacter, public ITDSActorInterface
{
    GENERATED_BODY()

public:
    ATDSCharacter();

    UFUNCTION(BlueprintCallable)  // delet! debug only
    bool GetIsAiming() const { return bIsAiming; };

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
    USpringArmComponent* SpringArm;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
    UCameraComponent* Camera;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UTDSInventoryComponent* InventoryComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UTDSPlayerHealthComponent* HealthComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
    float MinSpringArmLenght{600.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
    float MaxSpringArmLenght{1600.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (ClampMin = "1", ClampMax = "1000"))
    float DeltaLenght{100.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (ClampMin = "0.001", ClampMax = "1"))
    float RateDeltaLenght{0.02f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (ClampMin = "0.01", ClampMax = "1"))
    float StepDeltaLenght{0.03f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cursor")
    UDecalComponent* CursorToWorld;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement", Replicated)
    EMovementState MovementState{EMovementState::Run_State};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
    FCharacterSpeed MovementInfo;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cursor")
    UMaterialInterface* CursorMaterial{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cursor")
    FVector CursorSize{32.f, 32.f, 10.f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
    TArray<UAnimMontage*> DeathAnimations;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
    TMap<FString, UAnimMontage*> ShootingAnimationMap;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
    UAnimMontage* StunAnimation;

    virtual void BeginPlay() override;
    virtual void PostInitializeComponents() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
    virtual void Restart() override;

    virtual void Tick(float DeltaTime) override;

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    /* used in animation blueprint */
    UFUNCTION(BlueprintCallable)
    EMovementState GetCurrentMovementState() const { return MovementState; };

public:
    /* used in blueprint for spawning BP_WeaponPickup */
    UFUNCTION(BlueprintNativeEvent)
    void DropWeapon_BP(UMeshComponent* WeaponMesh, const FWeaponSlot& WeaponSlotInfo);
    /* for camera managment in blueprint */
    UFUNCTION(BlueprintCallable)
    UDecalComponent* GetCursorToWorld() const { return CursorToWorld; };

public:
    /* multiplayer */
    UFUNCTION(Server, Unreliable)
    void SetActorRotationByYaw_OnServer(float YawValue);
    UFUNCTION(NetMulticast, Unreliable)
    void SetActorRotationByYaw_Multicast(float YawValue);
    UFUNCTION(Server, Reliable)
    void SetMovementState_OnServer(EMovementState NewMovementState);
    UFUNCTION(NetMulticast, Reliable)
    void SetMovementState_Multicast(EMovementState NewMovementState);

private:
    float CurrentSpringArmLenght;
    FTimerHandle SmoothLenghtTimet;
    bool bInProgress{false};
    bool bCanChange{true};

    bool bWantsToSprint{false};
    bool bWantsToWalk{false};

    UPROPERTY(Replicated)
    bool bIsAiming{false};

    FTimerHandle SwitchToRagdollTimer;
    UPROPERTY(Replicated)
    bool bCanMoveCursore{true};

    bool bCanRun{true};

    bool bHasShield{true};

    bool bIsAlive{true};

    void MoveForward(float Amount);
    void MoveRight(float Amount);

    void ChangeSpringArmLenght(float Amount);
    void SmoothSpringArmChangingLenght();

    void SpawnDecalAtLocationForLocalPlayer();

    void TickMovement(float DeltaTime);
    void UpdatePlayerRotation();
    void GetActorAndCursorScreenPosition(APlayerController* PlayerController,  //
                                         FVector2D& ActorScreenPosition,       //
                                         FVector2D& MouseScreenPosition);

    void CharacterUpdate();
    void ChangeMovementState();
    float GetMovementDirection() const;

    void OnStartWalking();
    void OnStopWalking();

    void OnStartSprint();
    void OnStopSprint();

    void OnStartAiming();
    void OnStoptAiming();

    void TryToReloadWeapon();

    void CharacterAttackEvent(bool bIsFiring);

    UFUNCTION()
    void OnDeath();
    void SwitchToRagdoll();

    void OnStaminaIsOver(bool bIsStaminaOver);
    UFUNCTION()
    void OnShieldChanged(float ShieldCapacity);

    void ReloadingAnimationStarts(UAnimMontage* ReloadAnimation);
    void WeaponMakeShot(EWeaponShotType ShotType);
    UFUNCTION(Client, unreliable)
    void DestroyDecal_OnClient();
    UFUNCTION(NetMulticast, Unreliable)
    void PlayAnimMontage_Multicast(UAnimMontage* Animation);
    UFUNCTION(NetMulticast, Unreliable)
    void StopAnimMontage_Multicast(UAnimMontage* Animation);

    UFUNCTION(Server, Unreliable)
    void SetAimingState_OnServer(bool bEnabled);

private:
    /* interface functions ITDSActorInterface */
    virtual void MakeStun() override;
    virtual void ResetStun() override;
};
