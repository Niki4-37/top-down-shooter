// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "FuncLibrary/Types.h"
#include "TDSPlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameOverSignature);

UCLASS()
class TOPDOWNSHOOTER_API ATDSPlayerState : public APlayerState
{
    GENERATED_BODY()

public:
    /* Uses in Widget */
    UPROPERTY(BlueprintAssignable)
    FOnGameOverSignature OnGameOver;
    UPROPERTY(BlueprintAssignable)
    FOnIntegerValueChangedSignature OnScoreChanged;
    UPROPERTY(BlueprintAssignable)
    FOnIntegerValueChangedSignature OnLivesChanged;

    UFUNCTION(BlueprintPure)
    int32 GetLives() const { return Lives; };
    void DecreaseLive();
    void IncreaseLive();

    void AddScorePoints(int32 PointsToAdd);

    UFUNCTION(BlueprintPure)
    int32 GetScorePoints() const { return ScorePoints; };

    void SaveInventory(const TArray<FWeaponSlot>& WeaponsSlots, const TArray<FAmmoSlot>& AmmoSlots);
    void LoadInventory(TArray<FWeaponSlot>& WeaponsSlots, TArray<FAmmoSlot>& AmmoSlots);

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammunition")
    TArray<FWeaponSlot> Weapons;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammunition")
    TArray<FAmmoSlot> Ammo;

    UPROPERTY(ReplicatedUsing = LivesChanged_OnRep, EditDefaultsOnly, BlueprintReadWrite, Category = "GameRules")
    int32 Lives{2};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "GameRules")
    int32 MaxLives{10};

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
    UPROPERTY(ReplicatedUsing = ScorePointsChanged_OnRep)
    int32 ScorePoints;

    void UpdateLives(bool bIsDecreased);

    UFUNCTION()
    void LivesChanged_OnRep();

    UFUNCTION()
    void ScorePointsChanged_OnRep();

    UFUNCTION(Client, Reliable)
    void GameOver_OnClient();
};
