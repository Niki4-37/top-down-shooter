// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDSPlayerController.generated.h"

class UTDSRespawnComponent;

UCLASS()
class TOPDOWNSHOOTER_API ATDSPlayerController : public APlayerController
{
    GENERATED_BODY()

public:
    ATDSPlayerController();

    UFUNCTION(BlueprintCallable)
    bool IsPlayerSpectator() const;

    UFUNCTION(BlueprintImplementableEvent)
    void OnPlayerSpectator();

    virtual void OnRep_Pawn() override;

    void StartSpectating();

    UFUNCTION(Client, Reliable)
    void StartSpectating_OnClient();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UTDSRespawnComponent* RespawnComponent;

    virtual void OnPossess(APawn* InPawn) override;
    virtual void OnUnPossess() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
    UPROPERTY(Replicated)
    APawn* PawnToAttach{nullptr};

    APawn* GetPawnToAttachSpectator();
    void ApplyCameraEffects(const FVector& NewColorScale, const FLinearColor& FadeColor);
};
