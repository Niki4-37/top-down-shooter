// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FuncLibrary/Types.h"
#include "TDSGameModeBase.generated.h"

class ATDSSpawningActor;
class AAIController;

UCLASS()
class TOPDOWNSHOOTER_API ATDSGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATDSGameModeBase();

    //~~~~~DEBUG~~~~~~
    UPROPERTY(EditDefaultsOnly)
    uint8 PhaseIndex;
    //~~~~~~~~~~~~~~~~

    FOnIntegerValueChangedSignature OnProgressPointsChanged;
    FOnIntegerValueChangedSignature OnPhasePointsChanged;
    /* used in level blueprint */
    UPROPERTY(BlueprintAssignable)
    FOnGamePhaseChangedSignature OnGamePhaseChanged;

    virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
    virtual void StartPlay() override;
    virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;
    virtual AActor* ChoosePlayerStart_Implementation(AController* Player);

    void Killed(AController* VictimController, AController* KillerController);
    void TryToRespawn(AController* Controller);

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TSubclassOf<AAIController> AIControllerClass;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TArray<TSubclassOf<APawn>> AIPawnClasses;

private:
    // uint8 PhaseIndex{0};
    TArray<FGamePhaseData> GamePhaseDataArray;
    FGamePhaseData CurrentGamePhaseData;
    int32 GameProgressPoints{0};
    TMap<FName, TArray<ATDSSpawningActor*>> SpawningActorsMap;
    TArray<AAIController*> EnemyControllers;

    bool CanVictimPlayerRespawn(AController* VictimController);
    void UpdateKillerPlayerStatus(AController* KillerController, int32 Points);
    void SetupGamePhaseDataArray();
    void FillSpawningActorsMap();
    void StartRespawning(AController* Controller);
    void ResetPawn(const FName& PhaseName, AController* EnemyController);
    void ResetScorePointsAllAliveEnemies();
    void BeginNewPhase();
    void SpawnBoss();
};
