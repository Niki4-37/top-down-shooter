// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSRespawnComponent.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNSHOOTER_API UTDSRespawnComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UTDSRespawnComponent();

    void StartRespawning(float TimeToRespawn);

private:
    FTimerHandle RespawningTimer;

    void RespawnOwner();
};
