// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/Types.h"
#include "TDSHealthComponent.generated.h"

class UPhysicalMaterial;
class UTDSStateEffect;
class UNiagaraComponent;
class UNiagaraSystem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHealthChangedSignature, float, Health, float, MaxHealth, float, DeltaHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStateEffectAppliedSignature, FStateEffectData, StateEffect);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNSHOOTER_API UTDSHealthComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UTDSHealthComponent();

    UPROPERTY(BlueprintAssignable)
    FOnDeathSignature OnDeath;
    UPROPERTY(BlueprintAssignable)
    FOnHealthChangedSignature OnHealthChanged;
    UPROPERTY(BlueprintAssignable)
    FOnStateEffectAppliedSignature OnStateEffectApplied;

    void SetHealth(float NewHealth);

    UFUNCTION(BlueprintCallable)
    float GetHealth() const { return Health; };

    UFUNCTION(BlueprintCallable)
    bool IsDead() const { return Health <= 0.f; };

    UFUNCTION(BlueprintCallable)
    virtual void ReceiveDamage(float Damage, AController* InstigatedBy);

    UFUNCTION(BlueprintCallable)
    bool TryToAddHealth(float HealthAmount);

    bool IsHealthFull() const { return FMath::IsNearlyEqual(Health, MaxHealth); };

    void BoostHealth(float BoostFactor);

    void SetInvulnarable(bool Enabled) { bIsInvulnerable = Enabled; };

    UFUNCTION(BlueprintCallable)
    TArray<UTDSStateEffect*> GetAppliedStateEffects() const { return AppliedStateEffects; };
    void RemoveStateEffect(UTDSStateEffect* EffectToRemove);
    void AddNewStateEffect(UTDSStateEffect* EffectToAdd);  //! private

    bool TryToApplyStateEffect(UTDSStateEffect* EffectToApply);

    /* used in pickup */
    UFUNCTION(BlueprintCallable)
    void ApplyStateEffectFromPickup(TSubclassOf<UTDSStateEffect> StateEffect);

    UFUNCTION(BlueprintCallable)  // debug
    TArray<UNiagaraComponent*> GetVisualEffects() const { return StateEffectNiagaraComponents; };

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay rules")
    float DamageReducingFactor{1.f};

    UPROPERTY(Replicated)
    TArray<UTDSStateEffect*> AppliedStateEffects;
    UPROPERTY(ReplicatedUsing = AddNewStateEffect_OnRep)
    UTDSStateEffect* StateEffectToAdd{nullptr};
    UPROPERTY(ReplicatedUsing = RemoveStateEffect_OnRep)
    UTDSStateEffect* StateEffectToRemove{nullptr};

    virtual void BeginPlay() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
    virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags);

private:
    UPROPERTY()
    TArray<UNiagaraComponent*> StateEffectNiagaraComponents;
    UPROPERTY(Replicated)
    float Health{0.f};
    UPROPERTY(Replicated)
    float MaxHealth{100.f};

    bool bIsInvulnerable{false};

    UFUNCTION()
    void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

    UFUNCTION()
    void OnTakePointDamage(AActor* DamagedActor,
                           float Damage,
                           AController* InstigatedBy,
                           FVector HitLocation,
                           UPrimitiveComponent* FHitComponent,
                           FName BoneName,
                           FVector ShotFromDirection,
                           const UDamageType* DamageType,
                           AActor* DamageCauser);

    UFUNCTION()
    void OnTakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser);

    UPhysicalMaterial* GetOwnersPhysicalMaterial(AActor* CheckingActor);

    void Killed(AController* KillerController);

    UFUNCTION(Client, Unreliable)
    void OnHealthChanged_OnClient(float InHealth, float InMaxHealth, float DeltaHealth);

    UFUNCTION()
    void AddNewStateEffect_OnRep();
    UFUNCTION()
    void RemoveStateEffect_OnRep();
    UFUNCTION()
    void SpawnOrRemoveStateEffectFX(UTDSStateEffect* StateEffect, bool bIsAdd);

    void SpawnAndWritePermanentStateEffectFX(UNiagaraSystem* VisualEffect, FName BoneName);
    void RemovePermanentStateEffectFX(UNiagaraSystem* VisualEffect);
    UNiagaraComponent* SpawnStateEffectFX(UNiagaraSystem* VisualEffect, FName BoneName);

    UFUNCTION(NetMulticast, Unreliable)
    void SpawnStateEffectFX_Multicast(UNiagaraSystem* VisualEffect, FName BoneName);
};
