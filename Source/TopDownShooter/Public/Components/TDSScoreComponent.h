// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSScoreComponent.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNSHOOTER_API UTDSScoreComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UTDSScoreComponent();

    void SetScorePoints(int32 Points);
    int32 GetScorePoints() const { return ScorePoints; };
    void ResetScorePoints() { ScorePoints = 0; };

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

private:
    int32 ScorePoints{0};
};
