// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Components/TDSHealthComponent.h"
#include "FuncLibrary/Types.h"
#include "TDSPlayerHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShieldChangedSignature, float, ShieldCapacity);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStaminaChangedSignature, float, StaminaCapacity);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnStaminaIsOverSignature, bool);

class UNiagaraSystem;
class USoundCue;

UCLASS(meta = (BlueprintSpawnableComponent))
class TOPDOWNSHOOTER_API UTDSPlayerHealthComponent : public UTDSHealthComponent
{
    GENERATED_BODY()

public:
    UTDSPlayerHealthComponent();

    UPROPERTY(BlueprintAssignable)
    FOnShieldChangedSignature OnShieldChanged;
    UPROPERTY(BlueprintAssignable)
    FOnStaminaChangedSignature OnStaminaChanged;

    FOnStaminaIsOverSignature OnStaminaIsOver;

    void UpdateStaminaState(EMovementState State);

    UFUNCTION(BlueprintCallable)
    void ChangeStamina(float Points);

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Shield")
    float MaxShield{100.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
    float RestoreRate{0.2f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
    float CooldownDelay{5.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
    float ShieldModifier{1.f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    UNiagaraSystem* ShieldImpactEffect;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    USoundCue* ShieldImpactSound;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    UNiagaraSystem* ShieldDestroyEffect;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    USoundCue* ShieldDestroySound;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina")
    float MaxStamina{100.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
    float ReduceStaminaPoints{1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
    float RegenerationStaminaPoints{1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
    float StaminaRegenerationDelay{2.f};

    virtual void BeginPlay() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
    virtual void ReceiveDamage(float Damage, AController* InstigatedBy) override;

private:
    UPROPERTY(ReplicatedUsing = ShieldChanged_OnRep)
    float Shield{0.f};

    FTimerHandle ShieldCooldownTimer;

    float CurrentStamina;
    FTimerHandle RegenerationStaminaTimer;

    void RestoreShield();

    void StartStaminaRegeneration(const bool Enable);

    UFUNCTION()
    void ShieldChanged_OnRep();
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnShieldImpactFX_Multicast(UNiagaraSystem* Effect);
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnShieldImpactSound_Multicast(USoundCue* Sound);
};
