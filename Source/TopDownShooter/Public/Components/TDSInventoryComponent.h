// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/Types.h"
#include "TDSInventoryComponent.generated.h"

/* for inventory widget */
DECLARE_MULTICAST_DELEGATE_OneParam(FOnWeaponEquippedSignature, int8);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnInventoryAmmoChangedSignature, EWeaponType, int32);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnInventoryWeaponInfoChangedSignature, uint8, const FWeaponSlot&);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnAmmoInWeaponChangedSignature, uint8, int32);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnReloadindStartSignature, uint8);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnReloadindEndSignature, uint8);

class ATDSDefaultWeapon;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNSHOOTER_API UTDSInventoryComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UTDSInventoryComponent();

public:
    // clang-format off
    /* for inventory widget */
    FOnWeaponEquippedSignature              OnWeaponEquipped;
    FOnInventoryAmmoChangedSignature        OnInventoryAmmoChanged;
    FOnInventoryWeaponInfoChangedSignature  OnInventoryWeaponInfoChanged;
    FOnAmmoInWeaponChangedSignature         OnAmmoInWeaponChanged;
    FOnReloadindStartSignature              OnReloadindStart;
    FOnReloadindEndSignature                OnReloadindEnd;
    
    /* for Character's animation */
    FOnWeaponMakeShotSignature              WeaponMakeShot;
    FOnReloadingStartSignature                 ReloadingAnimationStarts;
    //clang-format on

protected:
    virtual void BeginPlay() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
    void SetWeaponStartFire(bool bIsFire);

    void TryToSwitchToNextWeapon();
    void TryToSwitchToPrevWeapon();

    void TryToReloadWeapon();

    void UpdateStateWeapon(EMovementState MovementState);

    void UpdateWeaponSlotsAdditionalData(uint8 WeaponSlotIndex, const FAdditionalWeaponData& Data);

    void LoadInventory();

    void SaveInventory();

    /* used in BP_TDSPickupAmmo */
    UFUNCTION(BlueprintCallable)
    bool UpdateAmmunition(EWeaponType Type, int32 DeltaAmmo);

    void OnReloadingStart(UAnimMontage* Animation);

    void OnReloadEnd(bool bIsSuccess, int32& AmmoToWeapon);

    void OnWeaponMakeShot(EWeaponShotType ShotType);

    /* used in BP_TDSPickupWeapon */
    UFUNCTION(Server, Reliable, BlueprintCallable)
    void AddWeaponToInventory_OnServer(AActor* ActorToPickup, const FWeaponSlot& NewWeaponSlotData);

    UFUNCTION(BlueprintCallable)
    TArray<FWeaponSlot> GetWeaponSlots() const { return WeaponSlots; };
    TArray<FAmmoSlot> GetAmmoSlots() const { return AmmoSlots; };

private:
    UPROPERTY(Replicated)
    ATDSDefaultWeapon* CurrentWeapon{nullptr};
    UPROPERTY(Replicated)
    int8 ActiveWeaponSlotIndex{0};

    bool bIsFirstStart{true};

    UPROPERTY(Replicated)
    TArray<FWeaponSlot> WeaponSlots;
    UPROPERTY(Replicated)
    TArray<FAmmoSlot> AmmoSlots;

    UFUNCTION(Server, Reliable)
    void InitInventory_OnServer();

    bool EquipWeaponFromSlot(int8& NewIndex);

    void SpawnWeapon(FName WeaponID, FAdditionalWeaponData Data);
    
    bool IsValidWeaponName(FName WeaponName);

    UFUNCTION(Server, Reliable)
    void TryToSwitchWeapon_OnServer(bool bIsNextWeapon);

    void TakeAmmoFromInventory(int32& AmmoToWeapon);

    bool FindWeponSlotIndexByWeaponID(const FName WeaponID, int32& WeaponSlotIndex);

    void DropWeapon(UMeshComponent* Mesh, const FWeaponSlot& WeaponSlotData);

    UFUNCTION(Client, Unreliable)
    void OnWeaponEquipped_OnClient(int8 SlotIndex);
    UFUNCTION(Client, Unreliable)
    void OnInventoryAmmoChanged_OnClient(EWeaponType Type, int32 Ammo);
    UFUNCTION(Client, Unreliable)
    void OnInventoryWeaponInfoChanged_OnClient(uint8 SlotIndex, const FWeaponSlot& WeaponSlot);
    UFUNCTION(Client, Unreliable)
    void OnAmmoInWeaponChanged_OnClient(uint8 WeaponSlotIndex, int32 AmmoInWeapon);
    UFUNCTION(Client, Unreliable)
    void OnReloadindStart_OnClient(uint8 WeaponSlotIndex);
};
