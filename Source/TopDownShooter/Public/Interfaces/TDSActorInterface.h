// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDSActorInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDSActorInterface : public UInterface
{
    GENERATED_BODY()
};

/**
 * for buff/debuff system
 */
class TOPDOWNSHOOTER_API ITDSActorInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.

public:
    virtual bool AvalableForEffects_CPP();

    virtual UPhysicalMaterial* GetPhysicalMaterial();

    virtual void MakeStun(){};
    virtual void ResetStun(){};

    virtual void GetScorePoints(int32& OutPoints) const {};
};
