// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSSpawningActor.generated.h"

class AAIController;

UCLASS()
class TOPDOWNSHOOTER_API ATDSSpawningActor : public AActor
{
    GENERATED_BODY()

public:
    ATDSSpawningActor();

    FName GetPhaseName() const { return PhaseName; };

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning System")
    FName PhaseName;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TArray<TSubclassOf<APawn>> PawnClasses;

    virtual void BeginPlay() override;
};
