// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FuncLibrary/Types.h"
#include "TDSStateEffect.generated.h"

UCLASS(BlueprintType, Blueprintable)
class TOPDOWNSHOOTER_API UTDSStateEffect : public UObject
{
    GENERATED_BODY()

public:
    FStateEffectData GetStateEffectData() const { return StateEffectData; };
    FName GetBoneToApplyEffectName() const { return BoneToApplyEffectName; };
    virtual void InitObject(AActor* ActorToApply, FName BoneName);
    virtual void DestroyObject();
    virtual bool IsSupportedForNetworking() const override { return true; };
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Buff Debuff Settings")
    FStateEffectData StateEffectData;

    UPROPERTY()
    AActor* ActorToApplyEffect{nullptr};

    bool bIsApplied{false};

private:
    UPROPERTY(Replicated)
    FName BoneToApplyEffectName;
};

UCLASS()
class TOPDOWNSHOOTER_API UTSDStateEffectExecuteOnce : public UTDSStateEffect
{
    GENERATED_BODY()

public:
    virtual void InitObject(AActor* ActorToApply, FName BoneName) override;
    virtual void DestroyObject() override;

    virtual void ExecuteOnce();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    float AmountValue{20.f};
};

UCLASS()
class TOPDOWNSHOOTER_API UTSDStateEffecExecuteInTime : public UTDSStateEffect
{
    GENERATED_BODY()

public:
    virtual void InitObject(AActor* ActorToApply, FName BoneName) override;
    virtual void DestroyObject() override;

    virtual void Execute();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    bool bIsChangeHealth{true};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    bool bCanStun{false};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    bool bIsBoost{false};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    bool bIsInvulnerable{false};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings", meta = (EditCondition = "bIsBoost", ClampMin = "0.01"))
    float BoostFactor{1.f};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings", meta = (EditCondition = "bIsChangeHealth"))
    float ExecutionRate{0.2f};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    float EffectDuration{5.f};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings", meta = (EditCondition = "bIsChangeHealth"))
    float AmountValue{2.f};

private:
    FTimerHandle DestroyTimer;
    FTimerHandle ExecutionTimer;
};

UCLASS()
class TOPDOWNSHOOTER_API UTSDPermanentStateEffect : public UTDSStateEffect
{
    GENERATED_BODY()

public:
    virtual void InitObject(AActor* ActorToApply, FName BoneName) override;
    virtual void DestroyObject() override;

    virtual void Execute();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings", meta = (ClampMin = "0"))
    float AmountValue{2.f};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    float ExecutionRate{0.4f};
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Debuff Settings")
    float AuraRadius{100.f};

private:
    FTimerHandle ExecutionTimer;
};