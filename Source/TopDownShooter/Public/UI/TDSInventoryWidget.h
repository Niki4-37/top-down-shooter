// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FuncLibrary/Types.h"
#include "TDSInventoryWidget.generated.h"

class UVerticalBox;
class UHorizontalBox;
class UActorComponent;

UCLASS()
class TOPDOWNSHOOTER_API UTDSInventoryWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    virtual void NativeOnInitialized() override;

protected:
    UPROPERTY(meta = (BindWidget))
    UVerticalBox* WeaponSlotsBox;

    UPROPERTY(meta = (BindWidget))
    UHorizontalBox* AmmoSlotsBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    TSubclassOf<UUserWidget> WeaponSlotWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    TSubclassOf<UUserWidget> AmmoSlotWidgetClass;

private:
    // uint8 ActiveWeaponSlotIndex{0};  // for loading inventory with saved active weapon slot index

    void OnNewPawn(APawn* NewPawn);
    void InitSlots();

    void OnWeaponEquipped(int8 ActiveWeaponSlot);
    void OnInventoryAmmoChanged(EWeaponType Type, int32 NewAmmo);
    void OnInventoryWeaponInfoChanged(uint8 WeaponSlotIndex, const FWeaponSlot& Data);
    void OnAmmoInWeaponChanged(uint8 WeaponSlotIndex, int32 NewAmmo);
    void OnReloadingStart(uint8 WeaponSlotIndex);
    void OnReloadingEnd(uint8 WeaponSlotIndex);
};