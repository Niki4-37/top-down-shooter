// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDSGameInterfaceWidget.generated.h"

class UHorizontalBox;
/**
 *
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSGameInterfaceWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    virtual void NativeOnInitialized() override;

protected:
    UPROPERTY(meta = (BindWidget))
    UHorizontalBox* InventoryBox;

private:
    void OnNewPawn(APawn* NewPawn);
};
