// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FuncLibrary/Types.h"
#include "TDSWeaponSlotWidget.generated.h"

class UTexture2D;
class UImage;
class UProgressBar;

UCLASS()
class TOPDOWNSHOOTER_API UTDSWeaponSlotWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    virtual void NativeOnInitialized() override;
    void InitWeaponSlot(const FWeaponSlot& Data);
    void UpdateAmmoCapacity(int32 NewAmmo);
    void StartReloadingProgress();
    void StopReloadingProgress();
    void ToggleWeaponStatus(bool bIsArmed);

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
    UTexture2D* FrameIcon{nullptr};

    UPROPERTY(meta = (BindWidget))
    UImage* WeaponImage;

    UPROPERTY(meta = (BindWidget))
    UProgressBar* AmmoCapacity;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor WeaponInArmsColor = FLinearColor::Blue;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor WeaponInHolsterColor = FLinearColor::Gray;

private:
    FWeaponSlot WeaponSlotData;

    float WeaponReloadingTime{0};

    FTimerHandle ReloadingTimer;
    FTimerHandle ReloadingOverTimer;

    void UpdateProgress();
};
