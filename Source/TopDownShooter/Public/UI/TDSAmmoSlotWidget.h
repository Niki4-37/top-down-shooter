// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FuncLibrary/Types.h"
#include "TDSAmmoSlotWidget.generated.h"

class UImage;
class UTextBlock;

UCLASS()
class TOPDOWNSHOOTER_API UTDSAmmoSlotWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    void InitAmmoSlot(const FAmmoSlot& Data);
    EWeaponType GetWeaponType() const { return AmmoSlotData.WeaponType; };
    void UpdateAmmoSlot(int32 NewAmmo);

protected:
    UPROPERTY(meta = (BindWidget))
    UImage* AmmoImage;

    UPROPERTY(meta = (BindWidget))
    UTextBlock* CarriedAmmoText;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor FullAmmoColor = FLinearColor::Green;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor NormalAmmoColor = FLinearColor::White;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    FLinearColor EmptyAmmoColor = FLinearColor::Red;

private:
    FAmmoSlot AmmoSlotData;
};
