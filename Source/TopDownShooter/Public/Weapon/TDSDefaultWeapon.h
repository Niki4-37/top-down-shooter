// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/Types.h"
#include "Delegates/Delegate.h"
#include "TDSDefaultWeapon.generated.h"

class USceneComponent;
class USkeletalMeshComponent;
class UStaticMeshComponent;
class UArrowComponent;

UCLASS()
class TOPDOWNSHOOTER_API ATDSDefaultWeapon : public AActor
{
    GENERATED_BODY()

public:
    ATDSDefaultWeapon();

    FOnReloadingStartSignature OnReloadStart;
    FOnReloadEndSignature OnReloadEnd;
    FOnWeaponMakeShotSignature OnWeaponMakeShot;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Settings")
    USceneComponent* SceneComponent{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Settings")
    USkeletalMeshComponent* SkeletalMeshComponent{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Settings")
    UStaticMeshComponent* StaticMeshComponent{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Settings")
    UArrowComponent* MuzzleLocationComp;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Settings")
    UArrowComponent* SleevEjectLocation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Settings")
    UArrowComponent* ClipEjectLocation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
    bool bIsSpawnProjectile{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
    FName WeaponID;

    UPROPERTY()
    FWeaponData WeaponData;

    UPROPERTY(Replicated)
    FAdditionalWeaponData AdditionalWeaponData;

    virtual void BeginPlay() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
    virtual void Tick(float DeltaTime) override;

    UFUNCTION(Server, Reliable)
    void SetWeaponStartFire_OnServer(bool bIsFiring);

    void SetWeaponData(const FWeaponData& Data);

    UFUNCTION(Server, Reliable)
    void UpdateStateWeapon_OnServer(EMovementState MovementState);

    void UpdateWeaponSpread();

    UFUNCTION(BlueprintCallable)
    FAdditionalWeaponData GetAdditionalWeaponData() const { return AdditionalWeaponData; };

    void SetAdditionalWeaponData(FAdditionalWeaponData Data) { AdditionalWeaponData = Data; };

    UFUNCTION(Server, Reliable)
    void ReloadWeapon_OnServer();

    void CancelReloading();

    // for user widget
    UFUNCTION(BlueprintCallable)
    FWeaponData GetWeaponData() const { return WeaponData; };
    UFUNCTION(BlueprintCallable)
    float ReturnRemainingReloadTime() const;
    UFUNCTION(BlueprintCallable)
    float ReturnRemainingLoadCartrigeTime() const;

    FName GetWeaponID() const { return WeaponID; };
    void SetWeaponID(const FName& WeaponName) { WeaponID = WeaponName; };

    UMeshComponent* GetWeaponMesh() const;

private:
    UPROPERTY(Replicated)
    bool bReloadInProgress{false};
    bool bIsCartrigeLoaded{true};
    bool bIsPlayerFiring{false};
    FTimerHandle ReloadingTimer;
    FTimerHandle AutomaticFireTimer;

    float CurrentWeaponSpread;
    float MinSpreadInState;
    float MaxSpreadInState;
    FTimerHandle ChangingSpreadTimer;

    FTimerHandle ClipEjectTimer;

    void InitWeapon();

    void MakeShot();

    void SpawnBullet();

    void ReduceAmmo();

    void CreateHitScanFXImpactEffect(const FHitResult& Hit);
    UFUNCTION(NetMulticast, Reliable)
    void SpawnImpactFX_Multicast(UNiagaraSystem* ImpactFX, const FVector& ImpactPoint, const FRotator& Rotation);
    UFUNCTION(NetMulticast, Reliable)
    void SpawnDecalAtLocation_Multicast(UMaterialInterface* Material, FVector Size, const FVector& ImpactPoint, const FRotator& Rotation, float DecalLifetime, float DecalFadeOutTime);
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnImpactSound_Multicast(USoundCue* Sound, const FVector& ImpactPoint);

    void CreateFXWeaponEffect();
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnMazzleFX_Multicast(UNiagaraSystem* MuzzleFX, const FTransform& MuzzleTransform);
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnSleevEjectFX_Multicast(UNiagaraSystem* SleevEjectFX);
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnShootingSound_Multicast(USoundCue* Sound);

    void LoadCartrigeAndShot();

    void MakeHitScan();
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnTraceFX_Multicast(UNiagaraSystem* TraceFX, const FVector& TraceEnd);

    void DealDamage(const FHitResult& HitResult);

    bool CanFire();

    void ReloadFinished();

    UFUNCTION()
    void ChangeSpread(int8 Modifier);

    void EjectClip();
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnClipEjectFX_Multicast(UNiagaraSystem* ClipEjectFX);
};
