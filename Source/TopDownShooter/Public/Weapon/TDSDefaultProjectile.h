// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/Types.h"
#include "TDSDefaultProjectile.generated.h"

class USphereComponent;
class UStaticMeshComponent;
class UProjectileMovementComponent;
class UNiagaraSystem;
class UPhysicalMaterial;

UCLASS()
class TOPDOWNSHOOTER_API ATDSDefaultProjectile : public AActor
{
    GENERATED_BODY()

public:
    ATDSDefaultProjectile();

    FProjectileData GetProjectileData() const { return ProjectileData; };

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile Settings")
    USphereComponent* CollisionComponent{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile Settings")
    UStaticMeshComponent* StaticMeshComponent{nullptr};

    UPROPERTY(VisibleDefaultsOnly, Category = "UProjectileMovementComponent")
    UProjectileMovementComponent* ProjectileMovementComponent{nullptr};

    UPROPERTY()
    TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;

    UPROPERTY()
    FProjectileData ProjectileData;

    virtual void BeginPlay() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
    virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;

public:
    virtual void Tick(float DeltaTime) override;

    UFUNCTION()
    virtual void ProjectileCollisionComponentHit(UPrimitiveComponent* HitComponent,  //
                                                 AActor* OtherActor,                 //
                                                 UPrimitiveComponent* OtherComp,     //
                                                 FVector NormalImpulse,              //
                                                 const FHitResult& Hit);

    // used in spawn deffered function!!!
    UFUNCTION(BlueprintCallable)
    virtual void InitProjectile(const FProjectileData& Data);

    // Make universal method for projectile and tracescan!!!
    void InitProjectileImpactEffects(const TMap<UPhysicalMaterial*, FImpactData>& Data);

    void SetShootDirection(FVector Direction) { ShootDirection = Direction; };

    virtual void ImpactProjectile();

private:
    FVector ShootDirection;

    UFUNCTION(NetMulticast, Reliable)
    void SetProjectileMeshAndTransform_Multicast(UStaticMesh* Mesh, FTransform LocalTransform);

    UFUNCTION(NetMulticast, Reliable)
    void UpdateProjectileSpeedAndVelocity_Multicast(float Speed, const FVector& Direction);

    UFUNCTION(NetMulticast, Reliable)
    void UpdateProjectileVelocity_Multicast(const FVector& Velocity);

    UFUNCTION(NetMulticast, Unreliable)
    void SpawnTraceFX_Multicast(UNiagaraSystem* TraceFX);

    UFUNCTION(NetMulticast, Reliable)
    void SpawnDecalAtLocation_Multicast(UMaterialInterface* Material, FVector Size, const FVector& ImpactPoint, const FRotator& Rotation, float DecalLifetime, float DecalFadeOutTime);

    UFUNCTION(NetMulticast, Reliable)
    void SpawnImpactFX_Multicast(UNiagaraSystem* ImpactFX, const FVector& ImpactPoint, const FRotator& Rotation);

    UFUNCTION(NetMulticast, Reliable)
    void SpawnImpactSound_Multicast(USoundCue* Sound, const FVector& ImpactPoint);
};