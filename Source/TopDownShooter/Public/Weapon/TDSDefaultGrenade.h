// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Weapon/TDSDefaultProjectile.h"
#include "TDSDefaultGrenade.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSDefaultGrenade : public ATDSDefaultProjectile
{
    GENERATED_BODY()

public:
    ATDSDefaultGrenade();

protected:
    virtual void BeginPlay() override;

    virtual void ProjectileCollisionComponentHit(UPrimitiveComponent* HitComponent,  //
                                                 AActor* OtherActor,                 //
                                                 UPrimitiveComponent* OtherComp,     //
                                                 FVector NormalImpulse,              //
                                                 const FHitResult& Hit) override;

    virtual void ImpactProjectile() override;

private:
    FTimerHandle ExplotionTimer;
    void Explode();

    UFUNCTION(NetMulticast, Reliable)
    void SpawnExplosionFX_Multicast(UNiagaraSystem* ExplosionFX);

    UFUNCTION(NetMulticast, Reliable)
    void SpawnExplosionSound_Multicast(USoundCue* Sound);
};
