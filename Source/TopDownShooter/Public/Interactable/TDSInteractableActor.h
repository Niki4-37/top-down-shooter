// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/TDSActorInterface.h"
#include "TDSInteractableActor.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSInteractableActor : public AActor, public ITDSActorInterface
{
    GENERATED_BODY()

public:
    ATDSInteractableActor();

protected:
    virtual void BeginPlay() override;

    virtual bool AvalableForEffects_CPP() override;

    virtual UPhysicalMaterial* GetPhysicalMaterial() override;
};
