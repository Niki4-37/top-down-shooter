// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

class UNiagaraSystem;
class UMaterialInterface;
class UPhysicalMaterial;
class UAnimMontage;
class USoundCue;
class UStaticMesh;
class ATDSDefaultProjectile;
class UTDSStateEffect;

// clang-format off
UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Aim_State       UMETA(DispayName = "Aim State"),
    AimWalk_State   UMETA(DispayName = "Aim Walk State"),
    Walk_State      UMETA(DispayName = "Walk State"),
    Run_State       UMETA(DispayName = "Run State"),
    Sprint_State    UMETA(DispayName = "Sprint State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
    RifleType           UMETA(DisplayName = "Rifle"),
    ShotgunType         UMETA(DisplayName = "Shotgun"),
    SniperRifleType     UMETA(DisplayName = "SniperRifle"),
    GrenadeLauncherType UMETA(DisplayName = "GrenadeLauncher")

};

UENUM(BlueprintType)
enum class EWeaponShotType : uint8
{
    LightShot,
    MediumShot,
    HeavyShot
};
// clang-format on

/* used in Default weapon class*/
DECLARE_MULTICAST_DELEGATE_OneParam(FOnReloadingStartSignature, class UAnimMontage*);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnReloadEndSignature, bool, int32&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnWeaponMakeShotSignature, EWeaponShotType);
/* used in GameMode */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnIntegerValueChangedSignature, int32, Score);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGamePhaseChangedSignature, FName, NewPhaseName);
/* used in GameState */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPercentChangedSignature, float, PecentValue);

USTRUCT(BlueprintType) struct FCharacterSpeed
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeed{300.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimWalkSpeed{100.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float WalkSpeed{200.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeed{600.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float SprintSpeed{800.f};
};

USTRUCT(BlueprintType)
struct FImpactData
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
    USoundCue* ImpactSound{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* ImpactEffect{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UMaterialInterface* DecalMaterial{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    FVector DecalSize = FVector(20.0f);

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    float DecalLifeTime = 5.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    float DecalFadeOutTime = 0.7f;
};

USTRUCT(BlueprintType)
struct FProjectileData
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
    TSubclassOf<ATDSDefaultProjectile> ProjectileClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    TSubclassOf<UTDSStateEffect> StateEffectClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile Settings")
    UStaticMesh* ProjectileMesh{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile Settings")
    FTransform MeshTransform;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
    float ProjectileDamage{10.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings", meta = (EditCondition = "!bIsGrenade"))
    float ProjectileLifetime{3.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
    float ProjectileInitialSpeed{2000.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
    float DamageZoneRadius{400.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
    float FullDamageRadius{200.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
    bool bIsGrenade{false};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade Settings", meta = (EditCondition = "bIsGrenade"))
    float ExplosionCountdown{3.f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    UNiagaraSystem* TraceFX{nullptr};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade Settings", meta = (EditCondition = "bIsGrenade"))
    UNiagaraSystem* ExplosionEffect{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
    USoundCue* ExplosionSound{nullptr};
};

USTRUCT(BlueprintType)
struct FWeaponSpread
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spread")
    float MinSpread{0.1f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spread")
    float MinSpreadAiming{0.05f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spread")
    float MaxSpread{1.0f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spread")
    float MaxSpreadAiming{0.5f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spread")
    float SpreadModifier{1.f};
};

USTRUCT(BlueprintType)
struct FWeaponData : public FTableRowBase
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
    TSubclassOf<class ATDSDefaultWeapon> WeaponClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    float RateOfFire{0.5f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    float ReloadTime{1.5f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    int32 MaxAmmo{10};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    bool bIsScuttergun{false};

    // if weapon has no bullets, only trace
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    bool bIsSpawnProjectile{false};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings", meta = (EditCondition = "!bIsSpawnProjectile"))
    float WeaponDamage{10};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings", meta = (EditCondition = "!bIsSpawnProjectile"))
    float DistanceTrace{2000.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
    FWeaponSpread WeaponSpread;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings" /*, meta = (EditCondition = "bIsSpawnProjectile")*/)
    FProjectileData ProjectileConfig;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
    USoundCue* ShootingSound{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
    USoundCue* ReloadingSound{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    UNiagaraSystem* MuzzleEffect{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    UNiagaraSystem* TraceEffect{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    FTransform MuzzleTransform;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    UNiagaraSystem* SleevEjectEffect{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VFX")
    UNiagaraSystem* ClipEjectEffect{nullptr};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "VFX")
    TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "VFX")
    float ClipEjectDelay{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
    EWeaponShotType ShotType{EWeaponShotType::LightShot};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
    UAnimMontage* ReloadingAnimation{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    UTexture2D* WeaponIcon{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammunition")
    EWeaponType WeaponType;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponData
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Ammo")
    int32 AmmoInWeapon{0};
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY()  // EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    UTexture2D* WeaponIcon{nullptr};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
    FName WeaponName = NAME_None;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
    FAdditionalWeaponData AdditionalData;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Info")
    int32 MaxAmmo{0};
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    UTexture2D* AmmoIcon{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammunition")
    EWeaponType WeaponType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammunition")
    int32 CarriedAmmo{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammunition")
    int32 MaxCarriedAmmo{0};
};

USTRUCT(BlueprintType)
struct FStateEffectData
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Buff Debuff Settings")
    FName StateEffectName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Buff Debuff Settings")
    bool bCanStack{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Buff Debuff Settings")
    bool bIsPermanentEffect{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Buff Debuff Settings")
    UTexture2D* StateEffecIcon{nullptr};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Buff Debuff Settings")
    TArray<UPhysicalMaterial*> PossibleInteractionMaterials;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Buff Debuff Settings")
    UNiagaraSystem* VisualEffect{nullptr};
};

USTRUCT(BlueprintType)
struct FGamePhaseData
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Rules")
    FName PhaseName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Rules", meta = (ClampMin = "0"))
    int32 ScoreToRichNextPhase{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Rules", meta = (ClampMin = "0"))
    int32 EnabledEnemiesNumber{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Rules")
    TSubclassOf<ACharacter> PhaseBossClass;
};

USTRUCT(BlueprintType)
struct FLevelData : public FTableRowBase
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Rules")
    TArray<FGamePhaseData> GamePhases;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    UTexture2D* LevelThumbnail{nullptr};
};

UCLASS()
class TOPDOWNSHOOTER_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    static void ApplyStateEffectByPhysicalMaterial(AActor* ActorToApplyEffect, TSubclassOf<UTDSStateEffect> EffectToApply, UPhysicalMaterial* FoundPhysMat, FName BoneName = NAME_None);
};
