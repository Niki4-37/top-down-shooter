// This is educational project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "FuncLibrary/Types.h"
#include "TDSGameStateBase.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSGameStateBase : public AGameStateBase
{
    GENERATED_BODY()

public:
    /* used in Game Data Widget */
    UPROPERTY(BlueprintAssignable)
    FOnPercentChangedSignature PercentProgerssChanged;

    UFUNCTION(BlueprintCallable)
    float GetPhaseProgress() const { return PhasePoints ? (static_cast<float>(ProgressPoints) / PhasePoints) : 0.f; };

    APawn* GetFirstAlivePawn() const;

protected:
    virtual void HandleBeginPlay() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
    UPROPERTY(ReplicatedUsing = ProgresPointsChanged_OnRep)
    int32 ProgressPoints{0};
    UPROPERTY(Replicated)
    int32 PhasePoints{0};

    UFUNCTION()
    void OnProgressPointsChanged(int32 Points);
    UFUNCTION()
    void OnPhasePointsChanged(int32 Points);

    UFUNCTION()
    void ProgresPointsChanged_OnRep();
};
